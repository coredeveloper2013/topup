<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\Method;
use App\Vmethod;
use App\Setup;
use App\Profile;
use App\Country;


class AdminController extends Controller
{
    public function adminHome(){
        $methods = Method::all();
        $vendors = User::where('role', 'vendor')->get();
        return view('admin.createVendor', compact('methods', 'vendors'));
    }
    public function adminCreateVendor(){
        $methods = Method::all();
        $vendors = User::where('role', 'vendor')->get();
        return view('admin.createVendor', compact('methods', 'vendors'));
    }
    public function postCreateVendor(Request $request){
        // dd($request);
        $this->validate($request,[
            'fname'   => 'required|string|max:255',
            'lname' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' =>'required|string:min:8|confirmed',
            'domain' =>'required|string',
            'Rave' =>'sometimes|nullable|string',
            'Paymentwall' =>'sometimes|nullable|string',
            'Paystack' =>'sometimes|nullable|string',
            'Voguepay' =>'sometimes|nullable|string',
            'phone'   =>'required|string',
            'address'   =>'required|string',
            'city'   =>'required|string',
            'zip'   =>'required|integer',
            'country'   =>'required|string',
        ]);
        $user = new User;
        $user->fname = $request->fname;
        $user->lname = $request->lname;
        $user->email = $request->email;
        $user->phone = $request->phone;
        $user->password = Hash::make($request->password);
        $user->domain = $request->domain;
        $user->role = 'vendor';
        $user->save();

        $setup = new Setup;
        $setup->user_id = $user->id;
        $setup->save();
        
        $profile = new Profile;
        $profile->user_id = $user->id;
        $profile->address = $request->address;
        $profile->city = $request->city;
        $profile->zip = $request->zip;
        $profile->country = $request->country;
        $profile->save();

        if($request->Rave){
            $rave = new Vmethod;
            $rave->name = $request->Rave;
            $rave->user_id = $user->id;
            $rave->save();
        }
        if($request->Paymentwall){
            $paymentwall = new Vmethod;
            $paymentwall->name = $request->Paymentwall;
            $paymentwall->user_id = $user->id;
            $paymentwall->save();
        }
        if($request->Paystack){
            $paystack = new Vmethod;
            $paystack->name = $request->Paystack;
            $paystack->user_id = $user->id;
            $paystack->save();
        }
        if($request->Voguepay){
            $voguepay = new Vmethod;
            $voguepay->name = $request->Voguepay;
            $voguepay->user_id = $user->id;
            $voguepay->save();
        }

        return redirect()->back()->with('success', "Vendor <strong>$user->fname $user->lname</strong> Created Successfully!!");
    }
    public function adminVendorEdit($id){
        $user = User::find($id);
        $methods = Method::all();
        return view('admin.vendorEdit', compact('user', 'methods'));
    }
    public function postUpdateVendor(Request $request, $id){
        // dd($request);
        $user = User::find($id);
        $this->validate($request,[
            'fname'   => 'required|string|max:255',
            'lname' => 'required|string|max:255',
            'phone' => 'required|string|max:255',
            'domain' =>'required|string',
            'Rave' =>'sometimes|nullable|string',
            'Paymentwall' =>'sometimes|nullable|string',
            'Paystack' =>'sometimes|nullable|string',
            'Voguepay' =>'sometimes|nullable|string',
        ]);

        $user->fname = $request->fname;
        $user->lname = $request->lname;
        $user->phone = $request->phone;
        $user->domain = $request->domain;
        $user->save();

        if($request->Rave){
            $rave = Vmethod::where('user_id', $user->id)->where('name', $request->Rave)->first();
            if(!$rave){
                $rave = new Vmethod;
                $rave->name = $request->Rave;
                $rave->user_id = $user->id;
                $rave->save();
            }
        }else{
            $delRave = Vmethod::where('user_id', $user->id)->where('name', 'Rave')->first();
            
            if($delRave){
                $delRave->delete();
            }
        }

        if($request->Paymentwall){
            $rave = Vmethod::where('user_id', $user->id)->where('name', $request->Paymentwall)->first();
            if(!$rave){
                $rave = new Vmethod;
                $rave->name = $request->Paymentwall;
                $rave->user_id = $user->id;
                $rave->save();
            }
        }else{
            $delRave = Vmethod::where('user_id', $user->id)->where('name', 'Paymentwall')->first();
            if($delRave){
                $delRave->delete();
            }
        }


        if($request->Paystack){
            $rave = Vmethod::where('user_id', $user->id)->where('name', $request->Paystack)->first();
            if(!$rave){
                $rave = new Vmethod;
                $rave->name = $request->Paystack;
                $rave->user_id = $user->id;
                $rave->save();
            }
        }else{
            $delRave = Vmethod::where('user_id', $user->id)->where('name', 'Paystack')->first();
            if($delRave){
                $delRave->delete();
            }
        }

        if($request->Voguepay){
            $rave = Vmethod::where('user_id', $user->id)->where('name', $request->Voguepay)->first();
            if(!$rave){
                $rave = new Vmethod;
                $rave->name = $request->Voguepay;
                $rave->user_id = $user->id;
                $rave->save();
            }
        }else{
            $delRave = Vmethod::where('user_id', $user->id)->where('name', 'Voguepay')->first();
            if($delRave){
                $delRave->delete();
            }
        }

        return redirect('/admin/create-vendor')->with('success', "Vendor <strong>$user->fname $user->lname</strong> Created Successfully!!");
    }
    public function adminInformation(){
        $country = Country::all();
        return view('admin.information', compact('country'));
    }
}

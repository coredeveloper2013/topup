<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Country;

class PageController extends Controller
{
    public function home(){
        $countries = Country::all();
        return view('pages.home', compact('countries'));
    }
    public function howToTopup(){
        $countries = Country::all();
        return view('pages.howToTopup', compact('countries'));
    }
    public function promotions(){
        return view('pages.promotions');
    }
    public function helpCenter(){
        return view('pages.helpCenter');
    }
    public function contact(){
        return view('pages.contact');
    }
    public function reviews(){
        return view('pages.reviews');
    }
    public function vendorLogin(){
        return view('pages.vendor-login');
    }
    public function adminLogin(){
        return view('pages.admin-login');
    }
    public function privacyPolicy(){
        return view('pages.privacy');
    }
    public function termCond(){
        return view('pages.term');
    }
}

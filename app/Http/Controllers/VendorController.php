<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Country;
use App\Operator;
use App\Product;
use App\Price;
use App\Setting;
use App\Transaction;
use App\Vmethod;
use App\Dummy;
use App\Content;
use App\Setup;
use Hash;
use Auth;
use Image;
use File;
use Session;

class VendorController extends Controller
{
    public function vendorHome(){
        $countries = Country::all();
        $session = null;
        if(Session::has('success'))  {                       
            $session = Session::get('success');
        }
        return view('vendor.home', compact('countries', 'session'));
    }
    public function vendorUsers(){
        $users = User::where('role', 'user')->where('vendor_id', Auth::id())->orderBy('id', 'desc')->get();
        return view('vendor.userList', compact('users'));
    }
    public function vendorUser($id){
        $user = User::find($id);
        $country = Country::all();
        return view('vendor.userView', compact('user', 'country'));
    }
    public function vendorCredentials(){
        // dd(Auth::user()->setup->rave);
        $methods = Vmethod::where('user_id', Auth::id())->orderby('queue', 'asc')->get();
        $idArr = $methods->pluck('name')->toArray();
        return view('vendor.settings', compact('methods', 'idArr'));
    }
    public function vendorActivateMethod($id){
        $methods = Vmethod::find($id);
        if($methods->active){
            $methods->active = 0;
        }else{
            $methods->active = 1;
        }
        $methods->save();
        return redirect()->back()->with('success', "Payment Gatewat <strong>$methods->name</strong> Activated Successfully");
    }
    public function userApiRave(Request $request){
        $this->validate($request,[
            'publicKey'   =>'required|string',
            'secretKey' =>'required|string',
            'currency' =>'required|string',
            'frequency' =>'required|integer',
            'order' =>'required|integer',
        ]);
        $rave = [
            'publicKey' => $request->publicKey,
            'secretKey' => $request->secretKey,
            'currency' => $request->currency,
            'frequency' => $request->frequency,
            'order' => $request->order,
        ];

        $vendor = Vmethod::where('user_id', Auth::id())->where('name', 'Rave')->first();
        $vendor->frequency = $request->frequency;
        $vendor->queue = $request->order;
        $vendor->save();
        // dd($vendor);
        $setup = Setup::firstOrCreate(['user_id' => Auth::id()]);
        if($setup){
            $setup->rave = serialize($rave);
            $setup->save();
            return redirect()->back()->with('success', 'Rave Setup Successful');
        }
        return redirect()->back()->with('warning', 'Something Went wrong');
    }
    public function userApiWall(Request $request){
        $this->validate($request,[
            'publicKey'   =>'required|string',
            'secretKey' =>'required|string',
            'currency' =>'required|string',
            'frequency' =>'required|integer',
            'order' =>'required|integer',
        ]);
        $paymentwall = [
            'publicKey' => $request->publicKey,
            'secretKey' => $request->secretKey,
            'currency' => $request->currency,
            'frequency' => $request->frequency,
            'order' => $request->order,
        ];
        $vendor = Vmethod::where('user_id', Auth::id())->where('name', 'Paymentwall')->first();
        $vendor->frequency = $request->frequency;
        $vendor->queue = $request->order;
        $vendor->save();
        // dd($vendor);
        $setup = Setup::firstOrCreate(['user_id' => Auth::id()]);
        if($setup){
            $setup->paymentwall = serialize($paymentwall);
            $setup->save();
            return redirect()->back()->with('success', 'Payment Wall Setup Successful');
        }
        return redirect()->back()->with('warning', 'Something Went wrong');
    }
    public function userApiStack(Request $request){
        $this->validate($request,[
            'publicKey'   =>'required|string',
            'secretKey' =>'required|string',
            'currency' =>'required|string',
            'frequency' =>'required|integer',
            'order' =>'required|integer',
        ]);
        $paystack = [
            'publicKey' => $request->publicKey,
            'secretKey' => $request->secretKey,
            'currency' => $request->currency,
            'frequency' => $request->frequency,
            'order' => $request->order,
        ];
        $vendor = Vmethod::where('user_id', Auth::id())->where('name', 'Paystack')->first();
        $vendor->frequency = $request->frequency;
        $vendor->queue = $request->order;
        $vendor->save();
        // dd($vendor);
        $setup = Setup::firstOrCreate(['user_id' => Auth::id()]);
        if($setup){
            $setup->paystack = serialize($paystack);
            $setup->save();
            return redirect()->back()->with('success', 'Pay Stack Setup Successful');
        }
        return redirect()->back()->with('warning', 'Something Went wrong');
    }
    public function userApiVogue(Request $request){
        $this->validate($request,[
            'publicKey'   =>'required|string',
            'secretKey' =>'required|string',
            'currency' =>'required|string',
            'frequency' =>'required|integer',
            'order' =>'required|integer',
        ]);
        $voguepay = [
            'publicKey' => $request->publicKey,
            'secretKey' => $request->secretKey,
            'currency' => $request->currency,
            'frequency' => $request->frequency,
            'order' => $request->order,
        ];

        $vendor = Vmethod::where('user_id', Auth::id())->where('name', 'Voguepay')->first();
        $vendor->frequency = $request->frequency;
        $vendor->queue = $request->order;
        $vendor->save();
        // dd($vendor);

        $setup = Setup::firstOrCreate(['user_id' => Auth::id()]);
        if($setup){
            $setup->voguepay = serialize($voguepay);
            $setup->save();
            return redirect()->back()->with('success', 'Vogue Pay Setup Successful');
        }
        return redirect()->back()->with('warning', 'Something Went wrong');
    }
    public function vendorPostApiCred(Request $request){
        $this->validate($request,[
            'userName'   =>'required|string',
            'password' =>'required|string',
        ]);
        $settings = Setting::firstOrCreate(['user_id' => Auth::id()]);
        $settings->apiUser = $request->userName;
        $settings->apiPassword = $request->password;
        $settings->save();
        return redirect()->back()->with('success', 'Credentials Saved Successfully');
    }
    public function userChangeStat($id){
        $user = User::find($id);
        if($user->status){
            $user->status = 0;
        }else{
            $user->status = 1;
        }
        $user->save();
        return redirect()->back()->with('success', 'User Disabled');
    }
    public function vendorPricing($c=null, $o=null){
        $countries = Country::all();

        $product = null;
        $c ? $operators = Operator::where('country_id', $c)->get() : $operators = null;
        $o ? $products = Product::where('operator_id', $o)->with('vendorPrice')->paginate(50) : $products = Product::with('vendorPrice')->paginate(50);
        // dd($products->first()->vendorPrice['price']);
        return view('vendor.pricing', compact('products', 'countries', 'operators', 'c','o'));
    }
    public function vendorSetCustomPrice(Request $request){
        $product = Product::find($request->id);
        if($product){
            $price = Price::where('product_id', $product->id)->where('user_id', Auth::id())->first();
            if($price){
                $price->product = $product->product;
                $price->price = $request->price;
                $price->save();
                return response()->json($price);
            }else{
                $price = new Price;
                $price->product_id = $request->id;
                $price->user_id = Auth::id();
                $price->product = $product->product;
                $price->price = $request->price;
                $price->save();
                
                return response()->json($price);
            }
        }
        return response()->json('Something Went Wrong');
    }
    public function vendorDeleteCustomPrice(Request $request){
        $product = Product::find($request->id);
        if($product){
            $price = Price::where('product_id', $product->id)->where('user_id', Auth::id())->first();
            $price->delete();
            return response()->json('Deleted Successfully');
        }
        return response()->json('Something Went Wrong');
    }
    public function vendorTransactions(){
        $transactions = Transaction::where('vendor_id', Auth::id())->orderby('id', 'desc')->get();
        // $topup = json_decode($transactions->first()->topup);
        // dd($topup->login);
        return view('vendor.transactioon', compact('transactions'));
    }
    public function vendorLogoChange(Request $r){
        $this->validate($r,[
            'logotext' =>'required|string',
       	]);
       	$user = Auth::user();
        if($r->hasFile('logo')){
            $file = $r->file('logo');
            $name =  $file->getClientOriginalName();
            $imageNames = $nameReplacer = time() .'-'.Auth::id(). '.' . $file->getClientOriginalExtension();

            $width = 190;
            $height = 80;
            $image = Image::make($file);
            $image->resize($width, $height, function ($constraint) {
                $constraint->aspectRatio();
            });
            $image->save('uploads/logos/'.$nameReplacer);


            $oldImg = $user->logo;
            $user->logo = $imageNames;
            if($oldImg){
                File::delete('uploads/logos/'.$oldImg);
            }
        }
        $user->logotext = $r->logotext;
        $user->save();
        return redirect()->back();
    }
    public function vendorInformation(){
        $country = Country::all();
        return view('vendor.information', compact('country'));
    }
    public function vendorPostSocial(Request $request){
        $this->validate($request,[
            'fb'   =>'sometimes|nullable|string',
            'tw' =>'sometimes|nullable|string',
        ]);

        $settings = Auth::user()->setting;
        $settings->fb = $request->fb;
        $settings->tw = $request->tw;
        $settings->save();
        return redirect()->back()->with('success', 'Social Links Updated');
    }
    public function postRechargeNowVendor(Request $request){
        $vendor = Auth::user();
        $topup = $vendor->setting;
        $trans = new Transaction;
        $trans->user_id = Auth::id();
        $trans->vendor_id = $vendor->id;
        $country = Country::find($request->country);
        // dd($request);
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://bosdia.com/TopUp/api/top_up",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "api_user_name=".$topup->apiUser."&api_password=".$topup->apiPassword."&destination_msisdn=".$request->phoneCode.$request->phone."&msisdn=".$vendor->email."&product=".$request->productField,
            CURLOPT_HTTPHEADER => array(
                "Content-Type"=> "application/json",
                "X-Requested-With"=> "XMLHttpRequest",
            ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);

        if ($err) {
            $trans->topup = $err;
        } else {
            $trans->topup = $response;
        }
        
        $recharge = $this->rechargeArray($request);

        $trans->billing = 'Self';
        $trans->recharge = serialize($recharge);
        $trans->response = 'Self';
        $trans->method = 'Self';
        $trans->pocket = 0.00;
        $trans->fullPocket = 1;
        $trans->save();

        return redirect()->back()->with('success', $trans);
    }











    private function rechargeArray($request){
        $country = Country::find($request->country);
        $product = Product::find($request->productId);
        $recharge = [
            'selCountryId' => $request->country,
            'selCountry' => $country->name,
            'selCurrency' => $product->pcur,
            'selOperator' => $request->operatorId,
            'selPhone' => $request->phoneCode.$request->phone,
            'selProduct' => $request->productField,
            'selProductPrice' => $request->productPriceField,
            'selProductPriceCurrency' => 'USD',
            'selProductId' => $request->productId,
        ];
        return $recharge;
    }
    public function insertCountry(){
        $dummy = Dummy::all();
        $grouped = $dummy->groupBy('country')->map(function ($value, $key) {
            return $value->map(function($value){
                return ['country' => $value->country, 'code' => $value->code];
            });
        });
        // dd($grouped);
        foreach($grouped as $g){
            foreach($g as $a){
                $country = Country::firstOrCreate(['name' => $a['country']], ['code' => $a['code']]);
            }
        }
    }
    public function mergeCountry(){
        $countries = Country::all();
        // dd($countries);
        foreach($countries as $c){
            $content = Content::where('name', $c->name)->first();
            if($content){
                $c->iso3 = $content->iso3;
                $c->iso2 = $content->iso2;
                $c->phonecode = $content->phonecode;
                $c->capital = $content->capital;
                $c->currency = $content->currency;
                $c->save();
            }
        }
    }
    public function insertOperator(){
        $dummy = Dummy::all();
        $grouped = $dummy->groupBy('operator')->map(function ($value, $key) {
            return $value->map(function($value){
                return ['name' => $value->operator, 'country' => $value->country];
            });
        });
        // dd($grouped);
        foreach($grouped as $g){
            foreach($g as $a){
                $country = Country::where('name', $a['country'])->first();
                $operator = Operator::firstOrCreate(['name' => $a['name']], ['country_id' => $country?$country->id:null,'country_name' => $a['country']]);
            }
        }
    }
    public function insertProduct(){
        $dummy = Dummy::all();
        
        foreach($dummy as $d){
            $operator = Operator::where('name', $d->operator)->first();
            $price = new Product;
            $price->operator_id = $operator->id;
            $price->pcur = $d->currency;
            $price->product = $d->product;
            $price->price = $d->price;
            $price->save();
        }
    }
}

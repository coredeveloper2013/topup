<?php

namespace App\Http\Controllers;

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

use Illuminate\Http\Request;
use App\User;
use App\Country;
use App\Operator;
use App\Product;
use App\Transaction;
use App\Pocket;
use App\Vmethod;
use Hash;
use Auth;
use Session;

class UserController extends Controller
{
    public function userHome(){
        $account = $this->vendor;
        // dd(Auth::user());
        return view('user.home', compact('account'));
    }
    public function userInformation(){
        $country = Country::all();
        return view('user.information', compact('country'));
    }
    public function rechargeNow($id=null){
        Session::forget('basic');
        $countries = Country::all();
        $cnt = null;
        if($id){
            $cnt = Country::find($id);
        }
        return view('user.rechargeNow', compact('countries', 'cnt'));
    }
    public function goToCheckout(){
        if(!Session::has('basic')){
            return redirect()->back();
        }
        
        $vendor = $this->vendor;
        $accounts = $this->vendor->setup;
        
        $rave = unserialize($accounts->rave);
        $wall = unserialize($accounts->paymentwall);
        $stack = unserialize($accounts->paystack);
        $vogue = unserialize($accounts->voguepay);
        // dd($stack);
        $mRave = Vmethod::where('user_id', $vendor->id)->where('name', 'Rave')->where('active', 1)->first();
        $mWall = Vmethod::where('user_id', $vendor->id)->where('name', 'Paymentwall')->where('active', 1)->first();
        $mStack = Vmethod::where('user_id', $vendor->id)->where('name', 'Paystack')->where('active', 1)->first();
        $mVogue = Vmethod::where('user_id', $vendor->id)->where('name', 'Voguepay')->where('active', 1)->first();

        $methods = Vmethod::where('user_id', $vendor->id)->where('active', 1)->orderby('queue', 'asc')->get();
        $selMethod = null;
        foreach($methods as $key => $m){
            if($m->frequency > 0){
                $selMethod = $m;
                break;
            }
        }

        $base = Session::get('basic');
        // dd($base);
        $pocket = Auth::user()->pocket->amount;

        $gretterPocket = false;
        $lessPocket = false;
        $cardAmmount = $base['productPriceField'];
        if($pocket > 0){
            $remaining = $pocket - $base['productPriceField'];
            if($remaining >= 0){
                $gretterPocket = true;
            }else{
                $lessPocket = true;
                $cardAmmount = $base['productPriceField'] - $pocket;
            }
        }
        // dd($lessPocket);
        // dd($base);
        $operator = Operator::find($base['operatorId']);
        $selCon = Country::find($operator->country_id);
        // dd($selCon);
        $country = Country::all();
        return view('user.goToCheckout', compact('country', 'base', 'operator', 'selCon', 'rave','wall','stack','vogue', 'vendor', 'gretterPocket', 'lessPocket', 'cardAmmount', 'selMethod'));
    }
    public function confirmOrder($id){
        if(!Session::has('basic')){
            return redirect()->route('rechargeNow');
        }
        $base = Session::get('basic');
        Session::forget('basic');
        $trn = Transaction::find($id);
        $operator = Operator::find($base['operatorId']);

        return view('user.confirmOrder', compact('base','trn','operator'));
    }
    
    public function userChangeEmail(Request $request){
        // dd($request);
        $this->validate($request,[
            'old_pass'   =>'required|string',
            'email' =>  'required|string|email|max:255|unique:users',
            'confirm_email' =>'required|string|email',
        ]);

        if($request->email !== $request->confirm_email){
            return redirect()->back()->with('warning', 'Email Address Doesnt Match');
        }
        $user = User::where('email', Auth::user()->email)->first();
        if(!Hash::check($request->old_pass, $user->password)){
            return redirect()->back()->with('warning', 'Password Doesn\'t Match');
        }

        Auth::user()->email = $request->email;
        Auth::user()->save();
        return redirect()->back()->with('success', 'Your Email Changed Successfully!!');
    }
    public function userChangePassword(Request $request){
        // dd($request);
        $this->validate($request,[
            'old_pass'   =>'required|string',
            'pass' =>  'required|string',
            'confirm_pass' =>'required|string',
        ]);

        if($request->pass !== $request->confirm_pass){
            return redirect()->back()->with('warning', 'Password Doesnt Match');
        }
        $user = User::where('email', Auth::user()->email)->first();
        if(!Hash::check($request->old_pass, $user->password)){
            return redirect()->back()->with('warning', 'Password Doesn\'t Match');
        }
        Auth::user()->password = bcrypt($request->pass);
        Auth::user()->save();
        return redirect()->back()->with('success', 'Your Password Changed Successfully!!');
    }
    public function userChangeTimezone(Request $request){
        // dd($request);
        $this->validate($request,[
            'account_timezone'   =>'required|string'
        ]);

        Auth::user()->timezone = $request->account_timezone;
        Auth::user()->save();
        return redirect()->back()->with('success', 'Your Timezone Changed Successfully!!');
    }
    public function userChangeProfile(Request $request){
        // dd($request);
        $this->validate($request,[
            'fname'   =>'required|string',
            'lname'   =>'required|string',
            'phone'   =>'required|string',
            'address'   =>'required|string',
            'city'   =>'required|string',
            'zip'   =>'required|string',
            'country'   =>'required|string',
        ]);

        Auth::user()->fname = $request->fname;
        Auth::user()->lname = $request->lname;
        Auth::user()->phone = $request->phone;
        Auth::user()->save();
        
        Auth::user()->profile->address = $request->address;
        Auth::user()->profile->city = $request->city;
        Auth::user()->profile->zip = $request->zip;
        Auth::user()->profile->country = $request->country;
        Auth::user()->profile->save();

        return redirect()->back()->with('success', 'Profile Information Updated Successfully!!');
    }
    public function userAccountDisabled(){
        return view('user.disabled');
    }
    public function getCountryOperator($code){
        $operators = Operator::where('country_id', $code)->get();
        if($operators){
            $view = view('ajax.operator',compact('operators'))->render();
            return response()->json($view);
        }
        return response()->json('Something went Wrong');
    }
    public function getOperatorProduct($id){
        $products = Product::where('operator_id', $id)->with('vendorPrice')->get();
        // dd($products->first()->vendorPrice['price']);
        if($products){
            $view = view('ajax.product',compact('products'))->render();
            return response()->json($view);
        }
        return response()->json('Something went Wrong');
    }
    public function postRechargeNow(Request $request){
        Session::put('basic', $request->input());
        return redirect()->route('goToCheckout');
    }
    public function userTransaction(){
        $vendor = $this->vendor;
        $transactions = Transaction::where('user_id', Auth::id())->where('vendor_id', $vendor->id)->orderby('id', 'desc')->get();

        return view('user.transaction', compact('transactions'));
    }
    public function userPocket(){
        $vendor = $this->vendor;
        $pocket = Pocket::where('user_id', Auth::id())->where('vendor_id', $vendor->id)->first();
        $transactions = Transaction::where('user_id', Auth::id())->where('vendor_id', $vendor->id)->orderby('id', 'desc')->get();
        // dd($pocket);
        return view('user.pocket', compact('transactions', 'pocket'));
    }
}

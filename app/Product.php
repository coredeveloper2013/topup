<?php

namespace App;
use Auth;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    // protected $with = ['operator'];
    public function operator(){
        return $this->belongsTo('App\Operator');
    }
    
    public function vendorPrice(){
        if(Auth::user()){
            if(Auth::user()->role == 'vendor'){
                return $this->hasOne('App\Price')->where('user_id', Auth::id());
            }elseif(Auth::user()->role == 'user'){
                $vendor = User::select('id', 'domain')->where('domain', url('/'))->first();
                return $this->hasOne('App\Price')->where('user_id', $vendor->id);
            }
        }else{
            $vendor = User::select('id', 'domain')->where('domain', url('/'))->first();
            return $this->hasOne('App\Price')->where('user_id', $vendor->id);
        }
    }
}

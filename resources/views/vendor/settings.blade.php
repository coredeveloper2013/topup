@extends('layouts.vendor')

@section('style')
<style>
    body.homepage #content, body.how-to-top-up #content{
        background-color: unset
    }
    body.homepage #wrapper{
        background-color: unset;
        background-image: unset;
    }
    body.homepage header, body.how-to-top-up header{
        background-color: rgba(2,122,195,1);
    }
</style>
@endsection
@section('content')
<section id="topSection">
        <div id="wrapper" class="clearfix">
            @include('partials._submenu')
            <div id="content">
                <div class="container-fluid margin_vertical20">
                    <div class="row-fluid">
                        @include('partials._message')
                    </div>
                    <div class="row-fluid">
                        <div class="span6">
                            <div class="border_all margin_bottom30 padding20 clearfix">
                                <h2>Topup Api Login Credentials</h2>
                                <div class="overflow_hidden change_info_dash">
                                    <div class="clearfix">
                                        <form id="change_credentials_password" action="{{ route('vendorPostApiCred') }}" method="post">
                                            @csrf
                                            <div id="change_pass_form" class="row">
                                            <input type="text" name="userName" placeholder="Api UserName" value="{{Auth::user()->setting?Auth::user()->setting->apiUser:null}}">
                                                <input type="password" name="password" placeholder="*******" value="{{Auth::user()->setting?Auth::user()->setting->apiPassword:null}}">
                                                <button type="submit" class="btn btn-primary pull-right">Save</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="border_all margin_bottom30 padding20 clearfix">
                                <h2>Social Links</h2>
                                <div class="overflow_hidden change_info_dash">
                                    <div class="clearfix">
                                        <form id="change_credentials_password" action="{{ route('vendorPostSocial') }}" method="post">
                                            @csrf
                                            <div id="change_pass_form" class="row">
                                                <input type="text" name="fb" placeholder="Facebook" value="{{Auth::user()->setting?Auth::user()->setting->fb:null}}">
                                                <input type="text" name="tw" placeholder="Twitter" value="{{Auth::user()->setting?Auth::user()->setting->tw:null}}">
                                                <button type="submit" class="btn btn-primary pull-right">Save</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="span6">
                            <div class="border_all margin_bottom30 padding20 clearfix">
                                <h2>Payment Methods</h2>
                                <div class="margin_top20">
                                    @foreach($methods as $m)
                                    <div class="clearfix margin_bottom10" id="notification_subcategory_email">
                                        <div class="margin_bottom5 margin_top5 pull-left width60">
                                            <strong style="padding: 5px; color:darkslateblue">{{$m->name}}</strong>
                                        </div>
                                        <div class="margin_bottom5 pull-left width40">
                                            <div class="pull-right">
                                                <a href="{{ route('vendorActivateMethod', $m->id) }}" class="btn {{$m->active ? 'btn-success':'btn-danger'}} btn-sm" style="padding: 0px 8px;font-size: 12px;" >
                                                    {{$m->active ? 'Activated':'Not Activated'}}
                                                </a>
                                            </div>
                                            @if($m->active)
                                            <div class="pull-right">
                                                <button class="btn btn-info btn-sm openSetupForm" style="padding: 0px 8px;font-size: 12px;margin:0px 5px" data-setup="{{$m->name}}">Setup</button>
                                            </div>
                                            @endif
                                        </div>
                                    </div>
                                    @endforeach
                                    @if(in_array('Rave', $idArr))
                                    <div class="clearfix margin_bottom10 padding10" id="setupRave" style="background-color:#dcdcdc;display:none">
                                            <h2>Rave Credential Setup</h2>
                                        <form class="form-horizoantal clearfix" action="{{route('userApiRave')}}" method="post">
                                            @csrf
                                            <div class="width100">
                                                <label for="">Api Public Key</label>
                                                <input type="text" name="publicKey" placeholder="Api Public Key" value="{{ Auth::user()->Setup ? unserialize(Auth::user()->setup->rave)['publicKey'] ? unserialize(Auth::user()->setup->rave)['publicKey']:null:null }}" />
                                            </div>
                                            <div class="width100 margin_left2">
                                                <label for="">Api Secret Key</label>
                                                <input type="text" name="secretKey" placeholder="Api Secret Key" value="{{ Auth::user()->Setup ? unserialize(Auth::user()->setup->rave)['secretKey'] ? unserialize(Auth::user()->setup->rave)['secretKey']:null:null }}" />
                                            </div>
                                            <div class="width30 pull-left margin_left2">
                                                <h2 style="margin-bottom:0px">Currency.</h2>
                                                <select name="currency" class="small_mobile_width32 width_auto">
                                                    <option value="USD" {{ Auth::user()->Setup ? unserialize(Auth::user()->setup->rave)['currency'] == "USD" ? 'selected':null:null }}>USD</option>
                                                    <option value="NGN" {{ Auth::user()->Setup ? unserialize(Auth::user()->setup->rave)['currency'] == "NGN" ? 'selected':null:null }}>NGN</option>
                                                </select>
                                            </div>
                                            <div class="width30 pull-left margin_left2">
                                                <h2 style="margin-bottom:0px">Frequency</h2>
                                                <input type="number" value="{{ Auth::user()->Setup ? unserialize(Auth::user()->setup->rave)['frequency'] ? unserialize(Auth::user()->setup->rave)['frequency'] :null:null }}" name="frequency">
                                            </div>
                                            <div class="width30 pull-left margin_left2">
                                                <h2 style="margin-bottom:0px">Order</h2>
                                                <input type="number" value="{{ Auth::user()->Setup ? unserialize(Auth::user()->setup->rave)['order'] ? unserialize(Auth::user()->setup->rave)['order'] :null:null }}" name="order">
                                            </div>
                                            <div class="clearfix"></div>
                                            <button type="submit" class="btn btn-primary pull-right">Save</button>
                                        </form>
                                    </div>
                                    @endif
                                    @if(in_array('Paymentwall', $idArr))
                                    <div class="clearfix margin_bottom10 padding10" id="setupPaymentwall" style="background-color:#dcdcdc;display:none">
                                            <h2>Payment Wall Credential Setup</h2>
                                        <form class="form-horizoantal clearfix" action="{{route('userApiWall')}}" method="post">
                                            @csrf
                                            <div class="width100">
                                                <label for="">Api Public Key</label>
                                                <input type="text" name="publicKey" placeholder="Api Public Key" value="{{ Auth::user()->Setup ? unserialize(Auth::user()->setup->paymentwall)['publicKey'] ? unserialize(Auth::user()->setup->paymentwall)['publicKey']:null:null }}" />
                                            </div>
                                            <div class="width100 margin_left2">
                                                <label for="">Api Secret Key</label>
                                                <input type="text" name="secretKey" placeholder="Api Secret Key" value="{{ Auth::user()->Setup ? unserialize(Auth::user()->setup->paymentwall)['secretKey'] ? unserialize(Auth::user()->setup->paymentwall)['secretKey']:null:null }}" />
                                            </div>
                                            <div class="width30 pull-left margin_left2">
                                                <h2 style="margin-bottom:0px">Currency.</h2>
                                                <select name="currency" class="small_mobile_width32 width_auto">
                                                    <option value="USD" {{ Auth::user()->Setup ? unserialize(Auth::user()->setup->paymentwall)['currency'] == "USD" ? 'selected':null:null }}>USD</option>
                                                    <option value="NGN" {{ Auth::user()->Setup ? unserialize(Auth::user()->setup->paymentwall)['currency'] == "NGN" ? 'selected':null:null }}>NGN</option>
                                                </select>
                                            </div>
                                            <div class="width30 pull-left margin_left2">
                                                <h2 style="margin-bottom:0px">Frequency</h2>
                                                <input type="number" value="{{ Auth::user()->Setup ? unserialize(Auth::user()->setup->paymentwall)['frequency'] ? unserialize(Auth::user()->setup->paymentwall)['frequency'] :null:null }}" name="frequency">
                                            </div>
                                            <div class="width30 pull-left margin_left2">
                                                <h2 style="margin-bottom:0px">Order</h2>
                                                <input type="number" value="{{ Auth::user()->Setup ? unserialize(Auth::user()->setup->paymentwall)['order'] ? unserialize(Auth::user()->setup->paymentwall)['order'] :null:null }}" name="order">
                                            </div>
                                            <div class="clearfix"></div>
                                            <button type="submit" class="btn btn-primary pull-right">Save</button>
                                        </form>
                                    </div>
                                    @endif
                                    @if(in_array('Paystack', $idArr))
                                    <div class="clearfix margin_bottom10 padding10" id="setupPaystack" style="background-color:#dcdcdc;display:none">
                                            <h2>Pay Stack Credential Setup</h2>
                                        <form class="form-horizoantal clearfix" action="{{route('userApiStack')}}" method="post">
                                            @csrf
                                            <div class="width100">
                                                <label for="">Api Public Key</label>
                                                <input type="text" name="publicKey" placeholder="Api Public Key" value="{{ Auth::user()->Setup ? unserialize(Auth::user()->setup->paystack)['publicKey'] ? unserialize(Auth::user()->setup->paystack)['publicKey']:null:null }}" />
                                            </div>
                                            <div class="width100 margin_left2">
                                                <label for="">Api Secret Key</label>
                                                <input type="text" name="secretKey" placeholder="Api Secret Key" value="{{ Auth::user()->Setup ? unserialize(Auth::user()->setup->paystack)['secretKey'] ? unserialize(Auth::user()->setup->paystack)['secretKey']:null:null }}" />
                                            </div>
                                            <div class="width30 pull-left margin_left2">
                                                <h2 style="margin-bottom:0px">Currency.</h2>
                                                <select name="currency" class="small_mobile_width32 width_auto">
                                                    <option value="USD" {{ Auth::user()->Setup ? unserialize(Auth::user()->setup->paystack)['currency'] == "USD" ? 'selected':null:null }}>USD</option>
                                                    <option value="NGN" {{ Auth::user()->Setup ? unserialize(Auth::user()->setup->paystack)['currency'] == "NGN" ? 'selected':null:null }}>NGN</option>
                                                </select>
                                            </div>
                                            <div class="width30 pull-left margin_left2">
                                                <h2 style="margin-bottom:0px">Frequency</h2>
                                                <input type="number" value="{{ Auth::user()->Setup ? unserialize(Auth::user()->setup->paystack)['frequency'] ? unserialize(Auth::user()->setup->paystack)['frequency'] :null:null }}" name="frequency">
                                            </div>
                                            <div class="width30 pull-left margin_left2">
                                                <h2 style="margin-bottom:0px">Order</h2>
                                                <input type="number" value="{{ Auth::user()->Setup ? unserialize(Auth::user()->setup->paystack)['order'] ? unserialize(Auth::user()->setup->paystack)['order'] :null:null }}" name="order">
                                            </div>
                                            <div class="clearfix"></div>
                                            <button type="submit" class="btn btn-primary pull-right">Save</button>
                                        </form>
                                    </div>
                                    @endif
                                    @if(in_array('Voguepay', $idArr))
                                    <div class="clearfix margin_bottom10 padding10" id="setupVoguepay" style="background-color:#dcdcdc;display:none">
                                            <h2>Vogue Pay Credential Setup</h2>
                                        <form class="form-horizoantal clearfix" action="{{route('userApiVogue')}}" method="post">
                                            @csrf
                                            <div class="width100">
                                                <label for="">Api Public Key</label>
                                                <input type="text" name="publicKey" placeholder="Api Public Key" value="{{ Auth::user()->Setup ? unserialize(Auth::user()->setup->voguepay)['publicKey'] ? unserialize(Auth::user()->setup->voguepay)['publicKey']:null:null }}" />
                                            </div>
                                            <div class="width100 margin_left2">
                                                <label for="">Api Secret Key</label>
                                                <input type="text" name="secretKey" placeholder="Api Secret Key" value="{{ Auth::user()->Setup ? unserialize(Auth::user()->setup->voguepay)['secretKey'] ? unserialize(Auth::user()->setup->voguepay)['secretKey']:null:null }}" />
                                            </div>
                                            <div class="width30 pull-left margin_left2">
                                                <h2 style="margin-bottom:0px">Currency.</h2>
                                                <select name="currency" class="small_mobile_width32 width_auto">
                                                    <option value="USD" {{ Auth::user()->Setup ? unserialize(Auth::user()->setup->voguepay)['currency'] == 'USD' ? 'selected':null:null }}>USD</option>
                                                    <option value="NGN" {{ Auth::user()->Setup ? unserialize(Auth::user()->setup->voguepay)['currency'] == "NGN" ? 'selected':null:null }}>NGN</option>
                                                </select>
                                            </div>
                                            <div class="width30 pull-left margin_left2">
                                                <h2 style="margin-bottom:0px">Frequency</h2>
                                                <input type="number" value="{{ Auth::user()->Setup ? unserialize(Auth::user()->setup->voguepay)['frequency'] ? unserialize(Auth::user()->setup->voguepay)['frequency'] :null:null }}" name="frequency">
                                            </div>
                                            <div class="width30 pull-left margin_left2">
                                                <h2 style="margin-bottom:0px">Order</h2>
                                                <input type="number" value="{{ Auth::user()->Setup ? unserialize(Auth::user()->setup->voguepay)['order'] ? unserialize(Auth::user()->setup->voguepay)['order'] :null:null }}" name="order">
                                            </div>
                                            <div class="clearfix"></div>
                                            <button type="submit" class="btn btn-primary pull-right">Save</button>
                                        </form>
                                    </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <script>
                    $('.openSetupForm').click(function(){
                        var id = $(this).attr('data-setup');

                        $('#setup'+id).slideToggle();
                    });
                </script>
            </div><!--end #content -->
        </div><!--end #wrapper -->
</section>
@endsection
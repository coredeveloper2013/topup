@extends('layouts.page')

@section('style')
<style>
    body.homepage #content, body.how-to-top-up #content{
        background-color: unset
    }
    body.homepage #wrapper{
        background-color: unset;
        background-image: unset;
    }
    body.homepage header, body.how-to-top-up header{
        background-color: rgba(2,122,195,1);
    }
    .hidden {
        display: none;
    }
    .pocketWrapper{
        text-align: center;
        margin-top: 100px;
    }
</style>
@endsection
@section('content')
<section id="topSection">
    <div id="wrapper" class="clearfix">
        @include('partials._submenu')
        <div id="content">
    <div class="container-fluid">
        <div class="row-fluid">
            <h1 class="margin_top20">Checkout</h1>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row-fluid">
            <div id="progress_bar_holder" class="margin_vertical20">
            <div class="progress_bar">
                <div class="actual_progress step3"></div>
            </div>
            <div class="clearfix">
                <div class="step pull-left active"></div><div class="step pull-right "></div>
            </div>
            </div>
        </div>
        <div class="row-fluid">
            <div class="border_all box_shadow margin_bottom30 padding40 mobile_padding10 clearfix overflow_hidden">
                    <div class="row-fluid">
                        <div class="span12">
                            <h2 class="margin_bottom15">Order Review</h2>
                            <table class="table table-products margin_bottom30 checkout_products border_all">
                                <thead>
                                    <tr>
                                        <th>Product</th>
                                        <th class="width80px mobile_pagination_center">Price</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="border_top">
                                        <td>
                                            <strong>Mobile Recharge</strong>
                                        </td>
                                        <td>
                                            <span dir="ltr">
                                                <strong id="ammToSend">${{ $base['productPriceField'] }}</strong>
                                            </span>
                                        </td>
                                        </tr>
                                    <tr>
                                        <td style="padding-top:5px; padding-bottom:5px;">
                                            <span class="padding_left20">
                                                Mobile number:
                                                <span dir="ltr" class="bold">{{ $base['phoneCode'] }}{{$base['phone']}}</span>
                                                <span class="italic font-size12"> - Please make sure this number is correct.</span>
                                            </span>
                                        </td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td style="padding-top:5px; padding-bottom:5px;">
                                            <span class="padding_left20">Product:
                                                <strong id="selAmmount">{{ $base['productCurrency'] }} {{ $base['productField'] }}</strong>
                                            </span>
                                        </td>
                                        <td></td>
                                    </tr>
                                    {{-- <tr>
                                        <td style="padding-top:5px; padding-bottom:5px;">
                                            <span class="padding_left20">Amount sent:
                                                <strong id="selAmmount">${{ $base['productPriceField'] }}</strong>
                                            </span>
                                        </td>
                                        <td></td>
                                    </tr> --}}
                                    <tr>
                                        <td style="padding-top:5px; padding-bottom:5px;">
                                            Operator:<span class="padding_left20" id="selOperator">{{$operator->name}}</span>
                                        </td>
                                        <td></td>
                                    </tr>
                                    <tr class="border_top">
                                        <td><span class="pull-right line_height40">Total</span></td>
                                        <td class="vertical_middle padding_vertical10"><span dir="ltr"><strong id="grndTot">${{ $base['productPriceField'] }}</strong></span></td>
                                    </tr>
                                    <tr class="border_top">
                                        <td>
                                            <span class="pull-right line_height40" style="width: 100%;text-align: right;">Pocket <i class="fa fa-question-circle" id="topupDefineShow" style="cursor: pointer"></i></span>
                                            <hr>
                                            <p id="topupDefine" style="font-size: 13px;display:none">The Pocket shows the amount of money you have on the system, if you perform a transaction and you make payment but the topup service fails then we put your payment in the pocket. Next time you are performing the same transaction or another one, we make use of the money in your pocket first.</p>
                                        </td>
                                        <td class="vertical_middle padding_vertical10"><span dir="ltr"><strong id="PocketAmount">${{Auth::user()->pocket->amount}}</strong></span></td>
                                    </tr>
                                    <tr class="border_top" style="display: none">
                                        <td><span class="pull-right line_height40">Converted Price</span></td>
                                        <td class="vertical_middle padding_vertical10"><span dir="ltr">NGN: <strong id="ngnConverted"></strong></span></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="row-fluid">
                        <div class="span6">
                            <div class="billing_information clearfix mobile_margin_bottom20">
                                <h2 class="margin_bottom15">Billing Information</h2>
                                <div class="width49 pull-left">
                                    <input type="text" name="bill[first_name]" id="i_first_name" value="{{Auth::user()->fname}}" placeholder="First Name" maxlength="50">
                                </div>
                                <div class="width49 margin_left2 pull-left">
                                    <input type="text" name="bill[last_name]" id="i_last_name" value="{{Auth::user()->lname}}" placeholder="Last Name" maxlength="50">
                                </div>
                                <div class="width100 pull-left">
                                    <input type="email" name="bill[email]" id="i_email" value="{{ substr(Auth::user()->email, strpos(Auth::user()->email, '-')+1) }}" placeholder="Email" maxlength="50">
                                </div>
                                <div class="width100 pull-left">
                                    <input type="tel" name="bill[phone]" id="i_phone" value="{{Auth::user()->phone}}" placeholder="Phone number" maxlength="50">
                                </div>
                                <div class="width100 pull-left">
                                    <input type="text" name="bill[address]" value="{{Auth::user()->profile->address}}" placeholder="Address" id="i_address" maxlength="100">
                                </div>
                                <div class="clearfix">
                                    <div class="width49 pull-left">
                                        <input type="text" name="bill[city]" id="i_city" value="{{Auth::user()->profile->city}}" class="last_child" placeholder="City" maxlength="50">
                                    </div>
                                    <div class="width49 margin_left2 pull-left">
                                        <input type="text" name="bill[zip]" id="i_zip" value="{{Auth::user()->profile->zip}}" placeholder="Zip Code" maxlength="50">
                                    </div>
                                </div>
                                <div class="width100 pull-left">
                                    <select class="width100" name="bill[country]" id="i_country">
                                        <option value="Choose Country">Choose Country</option>
                                        @foreach($country as $c)
                                            <option value="{{$c->id}}" {{Auth::user()->profile->country ? Auth::user()->profile->country == $c->iso2 ? 'selected': null:null}}>{{$c->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                {{-- <div class="width49 margin_left2 pull-left">
                                    <select class="width100" name="bill[state]" id="i_state" style="display: inline-block;">
                                        <option value="" selected="selected">Select region</option>
                                        <option value="Barisāl">Barisāl</option>
                                        <option value="Chittagong">Chittagong</option>
                                        <option value="Dhaka">Dhaka</option>
                                        <option value="Khulna">Khulna</option>
                                        <option value="Rājshāhi">Rājshāhi</option>
                                        <option value="Rangpur Division">Rangpur Division</option>
                                        <option value="Sylhet">Sylhet</option>
                                    </select>
                                    <input type="text" name="bill[other_state]" id="i_other_state" value="" class="none" placeholder="State/Province" maxlength="50" style="display: none;">
                                </div> --}}
                            </div>
                        </div>
                        <div class="span6">
                            <h2 class="margin_bottom15">Payment Method</h2>
                            @if(!$gretterPocket && $lessPocket)
                                <p style="background-color:darkkhaki; padding:5px">We will deduct 
                                    <strong>${{ Auth::user()->pocket->amount }}</strong> 
                                    from you <strong>Pocket</strong> and you will pay the remaining 
                                    <strong>${{ $cardAmmount }}</strong> 
                                    using your credit/debit card.
                                </p>
                            @endif
                            @if($gretterPocket)
                            <div class="pocketWrapper">
                                <button type="button" class="btn btn-success" id="payFromPocket">Pay From Pocket</button>
                                <p class="pocketLoading" style="display: none;color:crimson;text-align:center"><i class="fa fa-cog fa-spin" id="loadingTopup"></i> <br /> Processing Topup, Please Wait....</p>
                                <div id="pocketSuc" style="color:green"></div>
                                <div id="pocketErr" style="color:crimson"></div>
                            </div>
                            @endif
                            @if(!$gretterPocket)
                            <div class="payment_methods">
                                <ul class="clearfix">
                                    
                                </ul>
                                <div class="tab-content border_all padding20 " style="border-top-color:transparent; padding-top : 32px;">
                                    <div id="module_5" class="tab-pane payment_method_tab row-fluid border_box {{$selMethod ? $selMethod->name == 'Rave' ? 'active' : null: null}}" style="text-align:center">
                                        <div id="raveSuc" style="color:green"></div>
                                        <div id="raveErr" style="color:crimson"></div>
                                        <div class="lodingText" style="display:none;color:crimson;text-align:center"><i class="fa fa-cog fa-spin" id="loadingTopup"></i> <br /> Processing Topup, Please Wait....</div>
                                        <br>
                                        <div class="cardInfo">
                                            <form>
                                                <img src="{{ url('/img/VisaMaster.jpg') }}" alt="" style="margin-bottom: 20px">
                                                <script src="https://api.ravepay.co/flwv3-pug/getpaidx/api/flwpbf-inline.js"></script>
                                                <button type="button" class="btn btn-info" onClick="payWithRave()" style="margin: 0px auto;display: block">Pay with Master & Visa Cards</button>
                                            </form>
                                        </div>
                                    </div>
                                    <div id="module_15" class="tab-pane payment_method_tab row-fluid border_box {{ $selMethod ? $selMethod->name == 'Paymentwall' ? 'active':null: null }}">
                                        <div id="wallSuc" style="color:green"></div>
                                        <div id="wallErr" style="color:crimson"></div>
                                        <div class="lodingText" style="display: none;color:crimson;text-align:center"><i class="fa fa-cog fa-spin" id="loadingTopup"></i> <br /> Processing Topup, Please Wait....</div>
                                        <br>
                                        <div class="cardInfo">
                                            <img src="{{ url('/img/VisaMaster.jpg') }}" alt="" style="margin-bottom: 20px">
                                            <script src="https://api.paymentwall.com/brick/brick.1.3.js"> </script>
                                            <div id="payment-form-container"> </div>
                                        </div>
                                    </div>
                                    <div id="module_25" class="tab-pane payment_method_tab row-fluid border_box {{ $selMethod ? $selMethod->name == 'Paystack' ? 'active':null: null }}">
                                        <div id="stackSuc" style="color:green"></div>
                                        <div id="stackErr" style="color:crimson"></div>
                                        <div class="lodingText" style="display: none;color:crimson;text-align:center"><i class="fa fa-cog fa-spin" id="loadingTopup"></i> <br /> Pay with Master & Visa Cards</div>
                                        <br>
                                        <div class="cardInfo">
                                            <form >
                                                <img src="{{ url('/img/VisaMaster.jpg') }}" alt="" style="margin-bottom: 20px">
                                                <script src="https://js.paystack.co/v1/inline.js"></script>
                                                <button class="btn btn-info" type="button" onclick="payWithPaystack()" style="margin: 0px auto;display: block"> Pay with Master & Visa Cards </button> 
                                            </form>
                                        </div>
                                    </div>
                                    <div id="module_35" class="tab-pane payment_method_tab row-fluid border_box {{ $selMethod ? $selMethod->name == 'Voguepay' ? 'active':null: null }}">
                                        <div id="vogueSuc" style="color:green"></div>
                                        <div id="vogueErr" style="color:crimson"></div>
                                        <div class="lodingText" style="display: none;color:crimson;text-align:center"><i class="fa fa-cog fa-spin" id="loadingTopup"></i> <br /> Processing Topup, Please Wait....</div>
                                        <br>
                                        <div class="cardInfo">
                                            <img src="{{ url('/img/VisaMaster.jpg') }}" alt="" style="margin-bottom: 20px">
                                            <button class="btn btn-info" type="button" onclick="pay3()" style="margin: 0px auto;display: block">Pay with Master & Visa Cards</button>
                                        </div>
                                    </div>
                                    {{-- <div class="payment_extra_fields">
                                        <div id="save_cc_checkbox" class="clear margin_top10">
                                            <label class="margin0">
                                                <input id="save_payment_method" type="checkbox" name="save_payment_method" value="1" class="display_inlineblock"> Save this payment method
                                            </label>
                                        </div>
                                    </div> --}}
                                    </div>
                                </div>
                            </div>
                            @endif
                        </div>
                        {{-- <div class="row-fluid">
                            <div class="span12">
                                <div class="submit_holder pagination-centered mobile_margin_top10">
                                    <button type="submit" class="btn btn-primary">Place Order - &lrm;$11.73</button>
                                </div>
                            </div>
                        </div> --}}
                </div>
            </div>
        </div>
<script src="//voguepay.com/js/voguepay.js"></script>
<script>
    $.ajaxSetup({
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
    });
    const profileInfo = {
        fname: $('#i_first_name').val(),
        lname: $('#i_last_name').val(),
        email: $('#i_email').val(),
        phone: $('#i_phone').val(),
        address: $('#i_address').val(),
        city: $('#i_city').val(),
        zip: $('#i_zip').val(),
        country: $('#i_country').val(),

        selCountry: "{{ $base['country'] }}",
        selOperator: "{{ $base['operatorId'] }}",
        selPhone: "{{ $base['phoneCode'] }}{{ $base['phone'] }}",
        selProduct: "{{ $base['productField'] }}",
        selProductPrice: "{{ $base['productPriceField'] }}",
        selProductId: "{{ $base['productId'] }}",
    }



    // Convert Price
    endpoint = 'convert';
    access_key = 'e37e872ce028c84b652bca48dd6a910e';
    from = 'USD';to = 'NGN';amount = '{{ $cardAmmount }}';
    $.ajax({
        url: 'https://data.fixer.io/api/' + endpoint + '?access_key=' + access_key +'&from=' + from + '&to=' + to + '&amount=' + amount,   
        dataType: 'jsonp',
        success: function(json) {
            $('#ngnConverted').html(json.result);
        }
    });

    // Payment Wall
    setTimeout(function(){
        var getAmmount = '{{$wall['currency']}}' === 'NGN'? $('#ngnConverted').html() : {{ $cardAmmount }}
        var ammount = parseFloat(getAmmount);
        var brick = new Brick({
            public_key: "{{ $wall ? $wall['publicKey'] : '' }}",
            amount: ammount,
            currency: "{{ $wall ? $wall['currency'] : '' }}",
            container: 'payment-form-container',
            action: window.location.origin+"/api/transaction-wall/"+ammount+"/{{ $wall['currency'] }}",
            form: {
                merchant: 'Merchant Name',
                product: 'Product Name'
            }
        });

        brick.showPaymentForm(function(data) {
            console.log(data);
            profileInfo.dataAs = data.resas,
            profileInfo.txref = 'No ref for PaymentWall',
            profileInfo.selProductPriceCurrency = '{{ $wall ? $wall['currency'] : 'Unauthorized' }}',
            profileInfo.cardAmmount = {{$cardAmmount}};
            profileInfo.lessPocket = {{$lessPocket ? $lessPocket: 'null'}};
            $.ajax({
                type:'POST',
                url: window.location.origin+'/user/successfull-transaction-paymentwall',
                data:profileInfo,
                beforeSend: function ( xhr ) { 
                    $(".lodingText").show();
                    $(".cardInfo").hide();
                },
                success:function(data){
                    console.log(data);
                    $(".lodingText").hide();
                    var topup = JSON.parse(data.topup)

                    if(topup.transaction_state == 'error' && topup.reason){
                        $('#wallErr').html(topup.reason + '<a href="/user/pocket">Go To Pocket</a>')
                    }else if(topup.transaction_state == 'error' && topup.error){
                        $('#wallErr').html(topup.error + '<a href="/user/pocket">Go To Pocket</a>')
                    }else if(topup.login == 'failed'){
                        $('#wallErr').html(topup.transaction_state + ' <a href="/user/pocket"> Pocket</a>')
                    }else if(topup.transaction_state == 'success'){
                        $('#wallSuc').html('Transaction Successfull!!')
                    }
                    setTimeout(function () {
                        window.location.href = window.location.origin+"/user/confirm-order/"+data.id;
                    }, 2000);
                }
            });
        }, function(errors) {
            console.log('Wrong')
        });
    }, 4000);
    



    // VoguePay
    closedFunction=function() {
        
    }
     successFunction=function(transaction_id) {
        console.log('success. transaction ref is ' + transaction_id);
        profileInfo.txref = transaction_id,
        profileInfo.selProductPriceCurrency = '{{ $vogue ? $vogue['currency'] : 'Unauthorized' }}',
        profileInfo.cardAmmount = {{$cardAmmount}};
        profileInfo.lessPocket = {{ $lessPocket ? $lessPocket : 'null'}};
        $.ajax({
            type:'POST',
            url: window.location.origin+'/user/successfull-transaction-voguepay',
            data:profileInfo,
            beforeSend: function ( xhr ) { 
                     $(".lodingText").show();
                     $(".cardInfo").hide();
                },
            success:function(data){
                $(".lodingText").hide();
                console.log(data);
                var topup = JSON.parse(data.topup)

                if(topup.transaction_state == 'error' && topup.reason){
                    $('#vogueErr').html(topup.reason + '<a href="/user/pocket">Go To Pocket</a>')
                }else if(topup.transaction_state == 'error' && topup.error){
                    $('#vogueErr').html(topup.error + '<a href="/user/pocket">Go To Pocket</a>')
                }else if(topup.login == 'failed'){
                    $('#vogueErr').html(topup.transaction_state + ' <a href="/user/pocket"> Pocket</a>')
                }else if(topup.transaction_state == 'success'){
                    $('#vogueSuc').html('Transaction Successfull!!')
                }
                setTimeout(function () {
                    window.location.href = window.location.origin+"/user/confirm-order/"+data.id;
                }, 2000);
            }
        });
    }
     failedFunction=function(transaction_id) {
         alert('Transaction was not successful, Ref: '+transaction_id)
    }

     function pay3() {
         Voguepay.init({
             v_merchant_id: "{{ $vogue ? $vogue['publicKey'] : '' }}",
             total: '{{$vogue['currency']}}' === 'NGN'? $('#ngnConverted').html() : {{ $cardAmmount }},
             notify_url: 'http://domain.com/notification.php',
             cur: "{{ $vogue ? $vogue['currency'] : '' }}",
             merchant_ref: 'ref123',
             memo: 'Payment for Topup',
             developer_code: '5d3596ba7e506',
             store_id: 1,
             loadText:'Custom load text',
             items: [
                 {
                     name: "{{ $base['productField'] }}",
                     price: parseFloat('{{$vogue['currency']}}' === 'NGN'? $('#ngnConverted').html() : {{ $cardAmmount }})
                 },
             ],
             customer: {
                name: $('#i_first_name').val()+' '+$('#i_last_name').val(),
                address: $('#i_address').val(),
                city: $('#i_city').val(),
                state: 'Customer state',
                zipcode: $('#i_zip').val(),
                email: $('#i_email').val(),
                phone: $('#i_phone').val()
            },
             closed:closedFunction,
             success:successFunction,
             failed:failedFunction
         });
     }




    // Pasystack Payment Gateway
    function payWithPaystack(){
        const getAmmount = '{{$stack['currency']}}' === 'NGN'? $('#ngnConverted').html() : {{ $cardAmmount }}
        var ammount = parseInt(getAmmount) * 100;

        var handler = PaystackPop.setup({
            key: "{{ $stack ? $stack['publicKey'] : '' }}",
            email: $('#i_email').val(),
            amount: ammount,
            currency: '{{ $stack ? $stack['currency'] : '' }}',
            ref: ''+Math.floor((Math.random() * 1000000000) + 1),
            metadata: {
                custom_fields: [
                    {
                        display_name: $('#i_first_name').val(),
                        variable_name: $('#i_first_name').val()+' '+$('#i_last_name').val(),
                        value: $('#i_phone').val()
                    }
                ]
            },
            callback: function(response){
                console.log('success. transaction ref is ' + response.reference);
                profileInfo.txref = response.reference,
                profileInfo.selProductPriceCurrency = '{{ $stack ? $stack['currency'] : 'Unauthorized' }}',
                profileInfo.cardAmmount = {{$cardAmmount}};
                profileInfo.lessPocket = {{$lessPocket ? $lessPocket: 'null'}};
                $.ajax({
                    type:'POST',
                    url: window.location.origin+'/user/successfull-transaction-paystack',
                    data:profileInfo,
                    beforeSend: function ( xhr ) { 
                        $(".lodingText").show();
                        $(".cardInfo").hide();
                    },
                    success:function(data){
                        $(".lodingText").hide();
                        console.log(data);
                        var topup = JSON.parse(data.topup)

                        if(topup.transaction_state == 'error' && topup.reason){
                            $('#stackErr').html(topup.reason + '<a href="/user/pocket">Go To Pocket</a>')
                        }else if(topup.transaction_state == 'error' && topup.error){
                            $('#stackErr').html(topup.error + '<a href="/user/pocket">Go To Pocket</a>')
                        }else if(topup.login == 'failed'){
                            $('#stackErr').html(topup.transaction_state + ' <a href="/user/pocket"> Pocket</a>')
                        }else if(topup.transaction_state == 'success'){
                            $('#stackSuc').html('Transaction Successfull!!')
                        }
                        setTimeout(function () {
                            window.location.href = window.location.origin+"/user/confirm-order/"+data.id;
                        }, 2000);
                    }
                });
            },
            onClose: function(){
                alert('window closed');
            }
        });
        handler.openIframe();
    }
    // Rave Payment Gateway
    const API_publicKey = "{{ $rave ? $rave['publicKey'] : '' }}";
    function payWithRave() {
        var x = getpaidSetup({
            PBFPubKey: API_publicKey,
            customer_email: $('#i_email').val(),
            amount: '{{$rave['currency']}}' === 'NGN'? $('#ngnConverted').html() : {{ $cardAmmount }},
            customer_phone: $('#i_phone').val(),
            currency: "{{ $rave ? $rave['currency'] : '' }}",
            txref: "{{ $base['phoneNumber'] }}"+Math.floor((Math.random() * 1000000000) + 1),
            meta: [{
                metaname: "flightID",
                metavalue: "AP1234"
            }],
            onclose: function() {},
            callback: function(response) {
                var txref = response.tx.txRef;
                console.log("txref", txref);
                if (
                    response.tx.chargeResponseCode == "00" ||
                    response.tx.chargeResponseCode == "0"
                ) {

                    profileInfo.txref = txref;
                    profileInfo.selProductPriceCurrency = '{{ $rave ? $rave['currency'] : 'Unauthorized' }}',
                    profileInfo.cardAmmount = {{$cardAmmount}};
                    profileInfo.lessPocket = {{$lessPocket ? $lessPocket: 'null'}};
                    $.ajax({
                        type:'POST',
                        url: window.location.origin+'/user/successfull-transaction',
                        data:profileInfo,
                        beforeSend: function ( xhr ) { 
                            $(".lodingText").show();
                            $(".cardInfo").hide();
                        },
                        success:function(data){
                            $(".lodingText").hide();
                            console.log(data);
                            var topup = JSON.parse(data.topup)
                            console.log(topup.reason)

                            if(topup.transaction_state == 'error' && topup.reason){
                                console.log('a');
                                $('#raveErr').html(topup.reason + '<a href="/user/pocket">Go To Pocket</a>')
                            }else if(topup.transaction_state == 'error' && topup.error){
                                console.log('b');
                                $('#raveErr').html(topup.error + '<a href="/user/pocket">Go To Pocket</a>')
                            }else if(topup.login == 'failed'){
                                console.log('c');
                                $('#raveErr').html(topup.transaction_state + ' <a href="/user/pocket"> Pocket</a>')
                            }else if(topup.transaction_state == 'success'){
                                $('#raveSuc').html('Transaction Successfull!!')
                            }
                            setTimeout(function () {
                                window.location.href = window.location.origin+"/user/confirm-order/"+data.id;
                            }, 2000);
                        }
                    });
                } else {
                    // redirect to a failure page.
                }
                x.close(); // use this to close the modal immediately after payment.
            }
        });
    }
    
    $(document).on('click', '#payFromPocket', function() {
        $.ajax({
            type:'POST',
            url: window.location.origin+'/user/pocket-transaction',
            data:profileInfo,
            beforeSend: function(){
                $(".pocketLoading").show();
                $("#payFromPocket").hide();
            },
            success:function(data){
                $(".pocketLoading").hide();
                $("#payFromPocket").show();
                console.log(data);
                var topup = JSON.parse(data.topup)
                console.log(topup.reason)

                if(topup.transaction_state == 'error' && topup.reason){
                    console.log('a');
                    $('#pocketErr').html(topup.reason + '<a href="/user/pocket">Go To Pocket</a>')
                }else if(topup.transaction_state == 'error' && topup.error){
                    console.log('b');
                    $('#pocketErr').html(topup.error + '<a href="/user/pocket">Go To Pocket</a>')
                }else if(topup.login == 'failed'){
                    console.log('c');
                    $('#pocketErr').html(topup.transaction_state + ' <a href="/user/pocket"> Pocket</a>')
                }else if(topup.transaction_state == 'success'){
                    $('#pocketSuc').html('Transaction Successfull!!')
                }
                setTimeout(function () {
                    window.location.href = window.location.origin+"/user/confirm-order/"+data.id;
                }, 2000);
            }
        });
    });
    $('#topupDefineShow').click(function(){
        $('#topupDefine').slideToggle();
    });
        //  OLD --------------------------------------------------------------------------------------------

        $(window).on('load resize', function (){
            $('.paymenttabs1').height($('.paymenttabs0').height());
        });
        $(document).on('click', '.payment_methods a', function() {
            $('#payment_method').val($(this).attr('data-pm_id'));

            var has_authorize_by_reference = parseInt($(this).attr('data-has_authorize_by_reference'));

            if ($('.payment_method_tab.active select.pm_main').length > 0 && has_authorize_by_reference) {
                $('.payment_method_tab.active select.pm_main').change();
            } else {
                if (has_authorize_by_reference) {
                    $('#save_cc_checkbox').removeClass('none');
                } else {
                    $('#save_cc_checkbox').addClass('none');
                }
            }
        });
</script>
            </div><!--end #content -->
        </div><!--end #wrapper -->
    </section>
@endsection

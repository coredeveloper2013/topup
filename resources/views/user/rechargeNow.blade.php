@extends('layouts.page')

@section('style')
<style>
    body.homepage #content, body.how-to-top-up #content{
        background-color: unset
    }
    body.homepage #wrapper{
        background-color: unset;
        background-image: unset;
    }
    body.homepage header, body.how-to-top-up header{
        background-color: rgba(2,122,195,1);
    }
    .homepage .operators.selected>div{
        opacity: unset;
    }
    #errorModal{
        position: fixed;
    height: 200px;
    width: 400px;
    top: calc(50% - 100px);
    left: calc(50% - 200px);
    z-index: 9999;
    background-color: #bf5143;
    box-shadow: 0 0 19px #908080;
    padding: 50px 10px 5px;
    color: white;
    }
    #closErrorModal{
        position: absolute;
        right: 0;
        top: 0;
        padding: 15px;
        font-size: 25px;
        cursor: pointer;
    }
</style>
<script src="https://dclimolinks.com/QandRAuth/frontend/build/js//utils.js"></script>
<script src="https://dclimolinks.com/QandRAuth/frontend/build/js/intlTelInput.js"></script>

<link rel="stylesheet" href="https://dclimolinks.com/QandRAuth/frontend/build/css/intlTelInput.css">
@endsection
@section('content')
<section id="topSection">
        <div id="wrapper" class="clearfix">
            @include('partials._submenu')
                    <div id="content">
                <div class="container-fluid">
        <div class="row-fluid">
            <div class="span12">
                <h1 class="margin_top20">Recharge Mobiles Online</h1>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row-fluid">
            <div id="progress_bar_holder" class="margin_vertical20">
        <div class="progress_bar">
            <div class="actual_progress step1"></div>
        </div>
        <div class="clearfix">
            <div class="step pull-left active"></div>
            <div class="step pull-right "></div>
        </div>
        </div>
        </div>
        <div class="row-fluid">
            <div class="span9">
                <div class="mobile_margin_bottom15 margin_vertical10">
                    <div class="display_inline">
                                        </div>
                </div>
            </div>
        </div>
        <div class="mobile_margin_bottom15 margin_bottom50">
            <div class="row-fluid">
                <div class="span12">
                    <div class="mobile_border_none mobile_border_box_none border_all box_shadow padding40 mobile_padding0">
                        <form id="international_recharge_form" name="usa_recharge_form" action="{{ route('postRechargeNow') }}" method="post" class="position_relative">
                            @csrf
                            <input type="hidden" name="operatorId" id="operatorIdField" value="">
                            <input type="hidden" name="phoneNumber" id="phoneNumberField" value="">
                            <input type="hidden" name="productField" id="productField" value="">
                            <input type="hidden" name="productPriceField" id="productPriceField" value="">
                            <input type="hidden" name="productId" id="productId" value="">

                            <div id="noclick_overlay"></div>
                            <div class="padding_bottom20 mr_step step_ok">
                                <div class="row-fluid">
                                    <div class="span4 museo-500 font-size18 line_height40">Country</div>
                                    <div class="span8">
                                        <div class="input_country"></div>
                                        <select name="country" class="margin0 width80" id="international_country">
                                            <option value="">Select country</option>
                                            @foreach($countries as $country)
                                                <option value="{{$country->id}}" data-iso2="{{$country->iso2}}" data-id="{{$country->id}}" data-country_name="{{$country->name}}" data-phone-code="{{$country->phonecode}}" {{ $cnt ? $country->id == $cnt->id ? 'selected': '':'' }}>{{$country->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div id="operators_holder" class="row-fluid mr_step padding_vertical20 border_top step_ok">
                                <div class="clearfix margin_bottom20 ">
                                    <div class="row-fluid">
                                        <div class="span4 museo-500 font-size18 line_height65 margin_top5">Operator</div>
                                        <div class="span8 operators clearfix selected" id="operatorsHolder">
                                        </div>
                                    </div>
                                    <div id="many_operators_holder" class="pull-right pagination-centered display_block margin_top20 none"><a id="many_operators_trigger" href="#"></a></div>
                                </div>
                            </div>
                            <div id="phone_holder" class="padding_vertical20 border_top mr_step step_ok" style="z-index:100">
                                <div class="row-fluid">
                                    <div class="span4 museo-500 font-size18 line_height40">Phone number</div>
                                    <div class="span8">
                                        <div id="phone_fragments" class="clearfix" style="display:none">
                                            <div class="row">
                                                <div class="mr_phone_holder span8 max_on_tablet tablet_margin_bottom10" style="clear: both">
                                                    <input type="text" name="phoneCode" id="conCode" style="width:30%;float:left;text-align:right;color:darkgoldenrod"  readonly="readonly" required>
                                                    <input type="text" name="phone" id="phone" style="width:70%;float:left" required autocomplete="off">
                                                </div>
                                                {{-- <div id="nickname_span_holder" class="span4 max_on_tablet tablet_margin_left0">
                                                    <a id="nickname_text_holder" class="tablet_float_left pull-right padding_top10" href="#" onclick="return false">
                                                        Assign a name <i class="margin_left2pxl icon-info-sign cursor_help vertical_center mr_tooltip" rel="tooltip" tooltip-position="top" data-original-title="Add the name of the person whose mobile you're recharging to easily identify it next time."></i>
                                                    </a>
                                                    <div id="nickname_input_holder" class="none">
                                                        <div class="position_relative">
                                                            <input type="text" name="product_data[nickname]" autocomplete="off" class="mr_nickname margin_bottom0">
                                                            <i class="mr_tooltip_position margin_left2pxl icon-info-sign cursor_help vertical_center mr_tooltip" data-original-title="Add the name of the person whose mobile you're recharging to easily identify it next time."></i>
                                                            <div>Contact name</div>
                                                        </div>
                                                    </div>
                                                </div> --}}
                                            </div>
                                        </div>
                                        <div id="phone_error_message" class="clear margin_top20 margin_bottom0 alert alert-error none">The number entered is either incorrect or does not match the operator. Please verify it!</div>
                                    </div>
                                </div>
                            </div>
                            <div id="amount_holder" class="mr_step padding_top20 border_top step_ok">
                                <div class="row-fluid">
                                    <div class="span4 museo-500 font-size18 amount_text line_height65">Amount</div>
                                    <div class="span8">
                                        <div class="mr_preffered_rates" style="display:none;text-align: center;">
                                            <div data-operator_id="53" class="" id="productPlaceholder">

                                            </div>
                                            <button type="button" class="btn btn-success" id="loadMoreProd" style="text-align: center; margin-top:20px">Load All</button>
                                            <div class="details padding20 font-size16 pagination-centered border_all" style="display:none" id="selAmmountDisplay">
                                                Amount to be sent: <strong class="received-amount clsMainpageText01"></strong>
                                            </div>
                                            <div data-operator_name="Teletalk" data-operator_id="57" class="none"><div class="row-fluid"><div recharge-type="fixed" class="span4 border_all pagination-centered font-size24 mr_preffered_rate deselected" rate="9.79">$9.79</div><div recharge-type="fixed" class="span4 border_all pagination-centered font-size24 mr_preffered_rate deselected" rate="11.81">$11.81</div><div recharge-type="fixed" class="span4 border_all pagination-centered font-size24 mr_preffered_rate deselected" rate="13.75">$13.75</div></div><div class="row-fluid"><div recharge-type="fixed" class="span4 border_all pagination-centered font-size24 mr_preffered_rate deselected" rate="15.69">$15.69</div><div recharge-type="fixed" class="span4 border_all pagination-centered font-size24 mr_preffered_rate deselected" rate="17.64">$17.64</div><div recharge-type="fixed" class="span4 border_all pagination-centered font-size24 mr_preffered_rate deselected" rate="19.65">$19.65</div></div><div class="details padding20 font-size16 pagination-centered border_all"></div></div><div data-operator_name="Robi" data-operator_id="55" class="none"><div class="row-fluid"><div recharge-type="fixed" class="span4 border_all pagination-centered font-size24 mr_preffered_rate deselected" rate="9.79">$9.79</div><div recharge-type="fixed" class="span4 border_all pagination-centered font-size24 mr_preffered_rate deselected" rate="11.81">$11.81</div><div recharge-type="fixed" class="span4 border_all pagination-centered font-size24 mr_preffered_rate deselected" rate="13.75">$13.75</div></div><div class="row-fluid"><div recharge-type="fixed" class="span4 border_all pagination-centered font-size24 mr_preffered_rate deselected" rate="15.69">$15.69</div><div recharge-type="fixed" class="span4 border_all pagination-centered font-size24 mr_preffered_rate deselected" rate="17.64">$17.64</div><div recharge-type="fixed" class="span4 border_all pagination-centered font-size24 mr_preffered_rate deselected" rate="19.65">$19.65</div></div><div class="details padding20 font-size16 pagination-centered border_all"></div></div><div data-operator_name="Banglalink" data-operator_id="49" class="none"><div class="row-fluid"><div recharge-type="fixed" class="span4 border_all pagination-centered font-size24 mr_preffered_rate deselected" rate="9.79">$9.79</div><div recharge-type="fixed" class="span4 border_all pagination-centered font-size24 mr_preffered_rate deselected" rate="11.81">$11.81</div><div recharge-type="fixed" class="span4 border_all pagination-centered font-size24 mr_preffered_rate deselected" rate="13.75">$13.75</div></div><div class="row-fluid"><div recharge-type="fixed" class="span4 border_all pagination-centered font-size24 mr_preffered_rate deselected" rate="15.69">$15.69</div><div recharge-type="fixed" class="span4 border_all pagination-centered font-size24 mr_preffered_rate deselected" rate="17.64">$17.64</div><div recharge-type="fixed" class="span4 border_all pagination-centered font-size24 mr_preffered_rate deselected" rate="19.65">$19.65</div></div><div class="details padding20 font-size16 pagination-centered border_all"></div></div><div data-operator_name="CityCell" data-operator_id="51" class="none"><div class="row-fluid"><div recharge-type="fixed" class="span4 border_all pagination-centered font-size24 mr_preffered_rate deselected" rate="7.85">$7.85</div><div recharge-type="fixed" class="span4 border_all pagination-centered font-size24 mr_preffered_rate deselected" rate="9.79">$9.79</div><div recharge-type="fixed" class="span4 border_all pagination-centered font-size24 mr_preffered_rate deselected" rate="11.81">$11.81</div></div><div class="row-fluid"><div recharge-type="fixed" class="span4 border_all pagination-centered font-size24 mr_preffered_rate deselected" rate="15.69">$15.69</div><div recharge-type="fixed" class="span4 border_all pagination-centered font-size24 mr_preffered_rate deselected" rate="17.64">$17.64</div><div recharge-type="fixed" class="span4 border_all pagination-centered font-size24 mr_preffered_rate deselected" rate="19.65">$19.65</div></div><div class="details padding20 font-size16 pagination-centered border_all"></div></div><div data-operator_name="Airtel" data-operator_id="47" class="none"><div class="row-fluid"><div recharge-type="fixed" class="span4 border_all pagination-centered font-size24 mr_preffered_rate deselected" rate="7.85">$7.85</div><div recharge-type="fixed" class="span4 border_all pagination-centered font-size24 mr_preffered_rate deselected" rate="9.79">$9.79</div><div recharge-type="fixed" class="span4 border_all pagination-centered font-size24 mr_preffered_rate deselected" rate="11.81">$11.81</div></div><div class="row-fluid"><div recharge-type="fixed" class="span4 border_all pagination-centered font-size24 mr_preffered_rate deselected" rate="15.69">$15.69</div><div recharge-type="fixed" class="span4 border_all pagination-centered font-size24 mr_preffered_rate deselected" rate="17.64">$17.64</div><div recharge-type="fixed" class="span4 border_all pagination-centered font-size24 mr_preffered_rate deselected" rate="19.65">$19.65</div></div><div class="details padding20 font-size16 pagination-centered border_all"></div></div></div>
                                    </div>
                                </div>
                            </div>
                            
                            <div id="sms_notify" class="none padding_top20 margin_top20 notify_dest_holder border_top mr_step step_ok">
                                <div class="row-fluid ">
                                    <div class="span4 museo-500 font-size18">
                                        Send SMS									<img class="hidden-phone" style="margin-bottom:-70px;margin-left:-30px;width:30%" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGAAAABECAMAAABJe8AqAAAAXVBMVEUWRnQAAAAUQnAQOWPc4+kSPmoONF2bsMMGIEQNMFh4lK4HIkcLLVQKKVAGIEQGIEQGIUUGIEQIJUsGIEQJKE4GIETv8vWsvs5OcZT3+frR2uPF0d3l6u80XYb///8bcPDOAAAAHnRSTlOAAICA3YB/syd4oklwZwUTPRxVM18K7ryR99XM5oiddUo2AAACrElEQVRYw7XY23LjIAwGYCEVFYeDsbG3h237/o+5uN5U2yabQAL/Tcfjjj7AnmAE6kKi99as6q5cBJRhIl6MkrQCREDQNMSgJE0BlQgBkJzMojGgZg2wEX5UqguwOoQteoiqC6AMwWeQlrULoLyGPdpNHQCZQhYG0wNQiz4KyKkHIFMAJNsBUA5PhLZA1PCvMIW2QJg2QARNzOz8bJNZ7wVWE2fHpBF+BlFrIh4WK0otMEWfa/8ofuoQe2tCLRCMHWTgV5KRYTahAhgrqothp0LALCzVawz26ToQkicNNwbJxXAZMI5k8LcR5gIw+vvKy9Z0FgiRNTSI5hjOATL8lpMQwAwaWkU2DgESIzQMcvoORKnfSogC7F8/jYMUBTCM0DzI5giMA0KH4DD+BbyGLtF+ByJBp1DagHBmgR4+vvL76+rt6R1yDnLv4dt/Hs4s0pqBSFAE5Lwc6gCgmAGHpUAW3usAdAomhrPA48nV08f2dwN+ndz7X2iCqMuAXXiqBHSEpRx4vQFYwOFZQNZVlmi/Pny/J4/jXNDBAAXAMa9QCeTyXAE8vFcDnInrz0DeWKh8Brm8x7KH/JiF52oAPcy6DNjeobdqQM+QqBB4fslvaS1AEUYuBPZFqgR4BLVgGbAvUuVv0aJAGSoE9kWqAshkQDksBPZFqgDQqQ0w/XY08wmoudeePKsdWF0XQbt1Bzp9t+Awypfd1F7AYVICiNC2vgBZ0E3Xf6svgJw/mp9ABFDBciMB2QYlQPERs/6gKYBMQt9JoJbhCyAZZ9b3nTDn8UorYcoE3jz6+aRfAQ1aIdIOGQu7LSHNLEZZdZ5TqGlIBTPLPErbRWWAxFjHlBW81O8idtbc3hQMU7LLsDHZOUqI0rFLU2jQ1hxNsnbx3vFnnPeLtcmMqiB/AONvTCyrl0cuAAAAAElFTkSuQmCC">
                                    </div>
                                    <div class="span8">
                                        <label>
                                            <input id="sms_notify_trigger" type="checkbox" value="1" name="product_data[sms_notify]" class="display_inlineblock"> Let them know you recharged their mobile. Send a FREE SMS to: <strong class="notify_dest">+880 5695487584</strong>									</label>
                                        <div id="sms_notify_message" class="margin_top20 height0">
                                            <textarea name="product_data[sms_notify_message]" placeholder="Hey, I just recharged your mobile..."></textarea>
                                            <span id="sms_notify_char_left">160</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="extra_message_offer_holder" class="mr_step padding_top20 none">
                                <div class="row-fluid">
                                    <div class="span4 hidden-phone"></div>
                                    <div id="extra_message_offer" class="span8 pagination-centered font-size11"></div>
                                </div>
                            </div>
                            <div id="submit_holder" class="mr_step margin_top20 step_ok">
                                <div class="row-fluid">
                                    <div class="span4 hidden-phone"></div>
                                    <div class="span8 pagination-centered">
                                        <button type="submit" class="btn btn-primary padding_horizontal50" id="goToCheckout" disabled>Continue »</button>
                                        <div class="powered_by"></div>
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" value="" name="productCurrency" id="prodCur">
                        </form>
                    </div>
                                        <div style="margin-top:70px">&nbsp;</div>
                                </div>

            </div>
        </div>
    </div>
<div id="errorModal" style="display: none">
    <i class="fa fa-close" id="closErrorModal"></i>
    <h4>Enter the phone number in appropriate format.</h4>
    <h4>Example: Let's assume 07037483822 is a mobile number in United Kingdom</h4>
    <h4>You should enter 703748322 (omitting 0). Country code 44 is already preselected.</h4>
    <h4>This is not only for UK, the same thing is applicable to other countries.</h4>
</div>

<script>
        $('#closErrorModal').click(function(){
            $('#phone').val('')
            $('#goToCheckout').show();
            $('#errorModal').hide();
        })
        $('#loadMoreProd').click(function(){
            $('.gretterSIx').show();
            $('#loadMoreProd').hide();
        })

        $('#loadMoreProd').click(function(){
            $('.gretterSIx').show();
            $('#loadMoreProd').hide();
        })
        $('#phone').keyup(function(){
            if(this.value.charAt(0) === '0'){
                $('#goToCheckout').hide();
                $('#errorModal').show();
                this.val('');
            }
        })



    @if($cnt)
        var code = $('#international_country').find(':selected').attr('data-iso2');
        var id = $('#international_country').find(':selected').attr('data-id');
        $( '.input_country' ).addClass( 'flag32-' + code.toLowerCase() );

        console.log($('#international_country').find(':selected').attr('data-phone-code'));
        $('#conCode').val($('#international_country').find(':selected').attr('data-phone-code'));

        const operators = $('#operatorsHolder');
        const urlD = window.location.origin+'/user/get-operator-by-country/'+id;
        $( "#operatorsHolder" ).html('');

        $.ajax({
            url: urlD,
            contentType: "application/json",
            dataType: 'json',
            beforeSend :function(){
                $(".mr_input_spinner").show();
            },
            success: function(result){
                $(".mr_input_spinner").hide();
                $( "#operatorsHolder" ).append( result );
                
                $('#mr_phone_no').intlTelInput({
                    utilsScript: window.location.origin+'src/res/utils.js',
                    autoPlaceholder: true,
                    initialCountry : code.toLowerCase(),
                });
            }
        })
    @endif
</script>
<script>
    $( document ).ready(function() {
        $.ajaxSetup({
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
        });
        
        $('#international_country').change(async function () {
            var code = $(this).find(':selected').attr('data-iso2');
            var id = $(this).find(':selected').attr('data-id');
            $( '.input_country' ).addClass( 'flag32-' + code.toLowerCase() );

            console.log($(this).find(':selected').attr('data-phone-code'))
            $('#conCode').val($(this).find(':selected').attr('data-phone-code'))

            const operators = $('#operatorsHolder');
            const url = window.location.origin+'/user/get-operator-by-country/'+id;
            $( "#operatorsHolder" ).html('');

            $.ajax({
                url: url,
                contentType: "application/json",
                dataType: 'json',
                beforeSend :function(){
                    $(".mr_input_spinner").show();
                },
                success: function(result){
                    $(".mr_input_spinner").hide();
                    $( "#operatorsHolder" ).append( result );
                    
                    $('#mr_phone_no').intlTelInput({
                        utilsScript: window.location.origin+'src/res/utils.js',
                        autoPlaceholder: true,
                        initialCountry : code.toLowerCase(),
                    });
                }
            })
        });
        $(document).on( 'click', '.thisOperator', function () {
            $('.operator').removeClass('active');
            $('#loadMoreProd').show();
            $(this).addClass('active');
            const id = $(this).data('operator_id');
            const name = $(this).data('operator_fullname');
            const url = window.location.origin+'/user/get-products-by-operator/'+id;
            $( "#productPlaceholder" ).html('');
            $.ajax({
                url: url,
                contentType: "application/json",
                dataType: 'json',
                beforeSend :function(){
                    $(".mr_input_spinner_number").show();
                },
                success: function(result){
                    $(".mr_input_spinner_number").hide();
                    $('.mr_preffered_rates').show();
                    $('#operatorIdField').val(id);
                    $( "#productPlaceholder" ).append( result );
                }
            })
            $('#phone_fragments').show();
        });
        $(document).on( 'click', '.ammountToRecharge', function () {
            $('.ammountToRecharge').removeClass('selected').addClass('deselected');
            $(this).removeClass('deselected').addClass('selected');

            $("#goToCheckout").prop('disabled', false);

            $('#selAmmountDisplay').show();
            const product = $(this).attr('data-product');
            const currency = $(this).attr('data-currency');
            $('#prodCur').val(currency);
            const rate = $(this).attr('data-rate');
            const productId = $(this).attr('data-id');
            $('.received-amount').html(currency + ' ' + product);
            $('#productField').val(product);
            $('#productPriceField').val(rate);
            $('#productId').val(productId);
        });
});

function validPhoneNumber(){
    var cn =  $('#international_country').find(':selected').val();
    const flag = cn.toLowerCase()

    var isValidNumber = $("#mr_phone_no").intlTelInput("isValidNumber");
    if(!isValidNumber){
        $('#error-msg').show().html("Invalid Number");
        $('#valid-msg').hide();
        $('.mr_preffered_rates').hide();
        $('.check_mark').hide();
    } else {
        $('#error-msg').hide()
        $('#valid-msg').show().html('Valid Number');
        $('.mr_preffered_rates').show();
        var number = $("#mr_phone_no").val();
        $('#phoneNumberField').val(number);
        console.log('Valid Number');
        var countrySet = $('#international_country').find(':selected').attr('data-iso2');
        $("#mr_phone_no").intlTelInput("destroy");
        $('#mr_phone_no').intlTelInput({
            utilsScript: window.location.origin+'src/res/utils.js',
            autoPlaceholder: true,
            initialCountry : countrySet,
        });
    }
}
</script>
            </div><!--end #content -->
        </div><!--end #wrapper -->
    </section>
@endsection

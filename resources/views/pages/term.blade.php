@extends('layouts.page')
@section('style')
<style>
    body.homepage #content, body.how-to-top-up #content{
        background-color: unset
    }
    body.homepage #wrapper{
        background-color: unset;
        background-image: unset;
    }
    body.homepage header, body.how-to-top-up header{
        background-color: rgba(2,122,195,1);
    }
</style>
@endsection
@section('content')
<section id="topSection">
        <div id="wrapper" class="clearfix">
                @if(Auth::check())
                @if(Auth::user()->role = 'user')
                    @include('partials._submenu')
                @endif
                @endif
                    <div id="content">
                <script>
    var scroll_to = function(target) {
        var menu_height = 0;
    
        $.each($('#products_nav > *'), function(i, element) {
            menu_height += $(element).outerHeight();
        });
    
        $('html,body').animate(
            {
                scrollTop: $(target).offset().top - (menu_height + 10)
            },
            'slow',
            'swing'
        );
    };
    $(function() {
        if (location.hash != '')
            scroll_to(location.hash);
    });
    </script>
    <div class="container-fluid">
        <div class="row-fluid">
            <h1 class="margin_top20">Terms and Conditions</h1>
        </div>
    </div>
    <div class="container-fluid margin_top20 terms_and_conditions">
        <div class="row-fluid">
            <div class="span12">
                                <p>{{$_SERVER['SERVER_NAME']}} is a service of Miron Enterprises, LLC, one of the largest ethnic telecommunication providers in the world based in Atlanta, Georgia. </p>
                    <h1 style="margin-top:15px">General terms of use</h1>
                            <p>By visiting and shopping at {{$_SERVER['SERVER_NAME']}} you accept these Terms and Conditions.<br>&nbsp;<br> {{$_SERVER['SERVER_NAME']}}'s objective is to provide the highest quality services at the best rates on the market. The use of this website is expressly conditioned on your acceptance of all terms and conditions stated herein and any other place on this website. DO NOT USE THIS WEBSITE if you do not accept all the terms and conditions stated herein and at other places (including but not limited to: Frequently Asked Questions, Privacy Policy) on this website.</p>
                                <p>It is a federal AND state offense to purchase any product or service by fraudulent means. Products and/or services purchased on this website shall not be used for any unlawful purpose. By using our website, you represent that you are at least 13 years old. Persons who are at least 13 years of age but under the age of 18 may only use our Websites with legal parental or guardian consent. Accordingly, you agree that you are at least 18 years of age or older or possess legal parental or guardian consent, and are fully able and competent to enter into the terms, conditions, representations and warranties set forth in the Terms and Condition; otherwise, please exit the Website.</p>
                                <p>By using {{$_SERVER['SERVER_NAME']}}'s services the user accepts the rates, terms and conditions identified on this website. {{$_SERVER['SERVER_NAME']}} may change or modify the Terms from time to time without notice other then posting amended Terms on this Website. {{$_SERVER['SERVER_NAME']}} reserves the right to change, modify or discontinue, temporarily or permanently, the Website (or any portion thereof), including any and all content contained on the Website, at any time without notice. You agree that {{$_SERVER['SERVER_NAME']}} shall not be liable to you or to any third party for any modification, suspension or discontinuance of the Website.</p>
                                <p>Whenever you provide us information on our Website, you agree to: (a) provide true, accurate, current and complete information and (b) maintain and promptly update such information to keep it true, accurate, current and complete. If you provide any information that is, or we have reasonable grounds to suspect that it is, untrue, inaccurate, not current or incomplete, {{$_SERVER['SERVER_NAME']}} may without notice suspend or terminate your access to our Website and refuse any and all current or future use of our Website (or any portion thereof). Please select a password that would not be obvious to someone trying to guess your password, and change it regularly as an added precaution. Furthermore we recommend you use a different password from the email account as an additional security measure. You are responsible for maintaining the confidentiality of the password and account, and you are fully responsible for all activities that occur under your password or account identification. You should make every effort to safeguard your account data. {{$_SERVER['SERVER_NAME']}} is not responsible for lost or stolen account data or unauthorized use. You agree to immediately notify {{$_SERVER['SERVER_NAME']}} of any unauthorized use of your password or account or any other breach of security.</p>
                                <p>Credit/debit card fraud is a criminal offense. At {{$_SERVER['SERVER_NAME']}} we use automated and manual systems to confirm proper credit card authorization, including having {{$_SERVER['SERVER_NAME']}} representatives call persons who have signed up for {{$_SERVER['SERVER_NAME']}} services. We also track every transaction. Information related to fraudulent transactions, including but not limited to IP addresses, detailed transaction records and email addresses is collected. The information will be provided to appropriate law enforcement officials consistent with applicable law when required to assist in the prosecution of any persons attempting to commit fraud on {{$_SERVER['SERVER_NAME']}} websites.</p>
                                <p>{{$_SERVER['SERVER_NAME']}} is the sole owner of the information collected on this website. We will not sell, share, or rent this information to a third party for marketing or any other purposes. We collect, retain, and use only the information that is necessary for you to administrate your {{$_SERVER['SERVER_NAME']}} account. Therefore, it is required for you to provide certain information upon using portions of our websites. Such information includes your name (either your name, company, or organizations name), address, phone number, email address, and other information gathered through the applicable process.</p>
                                <p>{{$_SERVER['SERVER_NAME']}} does not store credit/debit card information. {{$_SERVER['SERVER_NAME']}} websites communicate over Secure Socket Layer protocol to ensure protection of your personal data. {{$_SERVER['SERVER_NAME']}} however shall not be responsible for the security of your data or for fraudulent use of credit card to make purchases on this website, nor shall {{$_SERVER['SERVER_NAME']}} be obligated to make refunds or to provide any compensation where such fraudulent use occurs. {{$_SERVER['SERVER_NAME']}} will actively cooperate with law enforcement authorities in prosecuting anyone who uses this website or products/services sold over this website for unlawful use. {{$_SERVER['SERVER_NAME']}} is not responsible if the card holder's issuing bank does not authorize online transactions.</p>
    <p> {{$_SERVER['SERVER_NAME']}} is committed to customer satisfaction and offers 100% money back guarantee. {{$_SERVER['SERVER_NAME']}} will refund the amount for any unused product to any customer who is dissatisfied with its services. The refund policy applies only for claims received by {{$_SERVER['SERVER_NAME']}} within 60 days since purchase date. Any promotion or discount applied to an order for which a refund is provided will be deducted from the amount of the refund.</p>
                                <p> By calling our Customer Service you agree that the call may be recorded for quality assurance or training purposes. The recordings may also be used in the remote event of fraud investigation. However, we do not sell, share or rent any of the audio recordings made during our order verification process or during standard customer queries.</p>
                                        </div>
        </div>
        <div class="row-fluid">
            <div class="span12">
                <p>{{$_SERVER['SERVER_NAME']}} MAKES NO WARRANTIES OR REPRESENTATIONS, EXPRESS OR IMPLIED, WHETHER BY FACT OR BY OPERATION OF LAW, IN CONTRACT OR TORT, STATUTORY OR OTHERWISE, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR USE OR PURPOSE OR REGARDING THIS WEBSITE, THE ACCURACY OR COMPLETENESS OF ANY INFORMATION PRESENTED IN THIS WEBSITE, OR ANY PRODUCT OR SERVICE SOLD OR PURCHASED THROUGH THIS WEBSITE.</p>                             <p>{{$_SERVER['SERVER_NAME']}} may discontinue, change, or suspend any aspect of the website at any time. {{$_SERVER['SERVER_NAME']}} may change, discontinue or suspend the availability of any database, content, feature, or product of the website. {{$_SERVER['SERVER_NAME']}} may impose limits on features, including services, use or restrict your access to parts or to the entire website without notice or liability.</p>                             <p>In no event shall {{$_SERVER['SERVER_NAME']}}, its employees, officers, representatives, service providers, suppliers, licensors, and agents be liable for any direct, special, indirect, incidental, exemplary, punitive or consequential damages, or any other damages of any kind, including but not limited to, loss of use, loss of profits, or loss of data, whether in an action in contract, tort (including but not limited to negligence), or otherwise, arising out of or in any way connected with (i) the use or inability to use the websites or the content, materials, software, information or transactions provided on or through the websites, or (ii) any claim attributable to errors, omissions, or other inaccuracies in the websites or the content, materials, software, information, products, or services on or available through the websites, (iii) the cost of procurement of substitute goods and services resulting from any products, data, information or services purchased or obtained or messages received or transactions entered into through or from our website; (iv) unauthorized access to or alteration of your transmissions or data; (v) statements or conduct of any third party on our website; (vi) the delay or failure in performance resulting from an act of force majeure, including without limitation, acts of god, natural disasters, communications failure, governmental actions, wars, strikes, labor disputes, riots, shortages of labor or materials, vandalism, terrorism, non-performance of third parties or any reasons beyond their reasonable control; or (vii) any other matter relating to our website, even if {{$_SERVER['SERVER_NAME']}} or its authorized representatives have been advised of the possibility of such damages. Your sole remedy for dissatisfaction with the website and/or website-related services is to stop using the website and/or those services.</p>                             <p>Dispute arising from purchase on this website or relating to this agreement, if not settled by the parties in good faith, may at their discretion, be submitted for arbitration to American Arbitration Association (AAA) at their offices in Georgia. All disputes, including interpretation of Arbitration Award shall be subject to laws of State of Georgia, and jurisdiction shall reside with Federal and State Court in Georgia. Prevailing party in the dispute shall be entitled to, in addition to any damages or relief, to reasonable expenses and attorney fees from the other party.</p> <p>Purchases will appear on your credit/debit card statement as “TopUp”. You agree not to file a credit card or debit card chargeback with regards to any purchase made on the website but instead try to resolve the issue with the service provider - {{$_SERVER['SERVER_NAME']}}. If you do not remember making the purchase - call the number on your statement next to the charge.<br>  In the event that you breach this agreement and file a chargeback, upon a resolution in our favor of the chargeback by either the credit card issuing bank, the credit/debit card processor or by VISA or MASTERCARD, you agree to reimburse us for any costs incurred in researching and responding to such chargeback, including without limitation, our actual costs paid to the credit/debit card processor or our banks and the reasonable value of the time of our employees spent on the matter, as determined in our discretion in good faith.<br>  In the event that a chargeback is placed or threatened on a purchase, we also reserve the right to report the incident for inclusion in chargeback abuser database(s). We use various credit reporting agencies and will send a default notice to them upon receiving a fraudulent order and/or chargeback. This in turn will seriously affect your credit rating. We are under no obligation to mark the debt as paid, even after we receive further payment to rectify the situation. We use several credit reporting agencies from Australia, United States, New Zealand, United Kingdom and several countries in Europe.</p>                             <p>These Terms and Conditions may be available in multiple languages; in the case of a conflict between any other language version and the English language version of these Terms and Conditions, the English language version shall always control.</p>                             		</div>
        </div>
                                        <div class="row-fluid" id="mobile_recharge">
                <div class="span12">
                    <h1 id="mobile_recharge">Top Up Service Agreement —Subject to change at any time in the sole discretion of the service provider</h1>
                                                <ul>
                                    <li>We are responsible for collecting all payments for orders you have placed on this website. Payment is authorized at the time the order is placed by you, and the payment will be taken from the bank account registered in your account. The rates for top ups are submitted to change without any prior notice.</li>
    <li>A processing fee of minimum $1 will apply to each transaction. We reserve the right to change our rates, fees or payment options at any time without notifying you in advance.</li>
    <li>This product is purely designed for residential usage. We will terminate the service agreement if otherwise suspected. This Agreement does not have a minimum or finite duration and will continue to be binding on the parties until it is terminated. We may terminate the Agreement at any time. We reserve the right to process or cancel any transactions in progress on termination of this Agreement or on suspension or withdrawal of the Service.</li>
    <li>{{$_SERVER['SERVER_NAME']}} is not responsible and will not offer any refunds for any loss you may incur in case of mistyping the recipient's phone number, choosing the incorrect operator or entering incorrect information.</li>
    <li>It may take up to 24 hours for the mobile operator to update the credit to the prepaid number you selected. </li>
    <li>Some countries apply local taxes beyond our control. If the country you send mobile credit to is one of them, the specific value that sums up the local taxes will be deducted from your order value, as displayed on the online ordering form on the website. Local taxes  are beyond the control of this service. They are assessed and levied by a local entity or authority such as a state, county, municipality, or a local operator in this case. These local taxes can include only the VAT, or the VAT and other fees established by the municipality, the carrier providing the local mobile service, or a national law. The processing fee is what  adds to the order value to cover for international transaction costs. The service processing fee will be displayed in the checkout, which is the last step in the purchase process. </li>
    <li>Some operators may apply VAT and taxes that will be deducted from the mobile recharge amount based on the local rules of destination network.</li>
    <li>Once the top up is accepted by the operator whose contact information is available on the invoice, the operator becomes responsible for completing the recharge.</li> 
    </ul>			</div>
            </div>
                        <div class="row-fluid">
                    <div class="span12">
                        <h1 id="nauta">Nauta Recharges Service Agreement — Subject to change at any time in the sole discretion of service provider</h1>
                        <ul>
                                    
    <li>A processing fee of minimum $1 will apply to each transaction. We reserve the right to change our rates, fees or payment options at any time without notifying you in advance.</li>
    <li>{{$_SERVER['SERVER_NAME']}} is not responsible and will not offer any refunds for any loss you may incur in case of mistyping the recipient's account or entering incorrect information.</li>
    <li>It may take up to 24 hours for Nauta to update the credit to the account you selected. </li>
    <li>Once the Nauta recharge is accepted, Nauta becomes responsible for updating the balance of the account.</li>
    <li>This product is purely designed for residential usage. We will terminate the service agreement if otherwise suspected. </li> 
    </ul>				</div>
                </div>
                            <div class="row-fluid">
            <div class="span12">
                <h1 id="service_accessibility">Service Accessibility
    </h1>
                <p><strong>Accessibility Records </strong><br>
    {{$_SERVER['SERVER_NAME']}} keeps records of any interaction established between the company and any impaired individual who contacted the company’s customer support team through any of the following methods: chat, email, phone, or postal mail. Records are registered and saved for an undefined period of time; they are confidential and will be used only to determine statistics, general trends, or recurrent requests of the customers. Records can also be used to analyze, improve &amp; develop features or options to further meet customers needs in regards to service accessibility and usability. <br><br>
    
    <strong>Accessibility features </strong><br>
    As a website owner, the company behind {{$_SERVER['SERVER_NAME']}} is responsible to adjust its features and services so that its website content is compatible with the majority of the browsers available on the market. Moreover, {{$_SERVER['SERVER_NAME']}} can be used with any of the rendering devices used by visually impaired individuals. The content is not encrypted and can be accessed by audio rendering programs.<br> 
    Any visually impaired individual can use their regular phone or special device to make use of our service &amp; applications, to the extent to which they are able to use their device and dial regular numbers such as: access numbers, PIN numbers, or numbers saved in their phone’s Contacts list.<br>
    
    We’re committed to offering accessibility support to our customers with vision, hearing, mobility, and speech limitations. Therefore, any question regarding the use or accessibility of our services can be solved over the phone or email. Our customer support team is prepared and able to accept calls that use audio rendering devices or any other special devices used by people with disabilities. Still, the services offered by {{$_SERVER['SERVER_NAME']}} can be purchased solely online through the paying platforms available on {{$_SERVER['SERVER_NAME']}}. <br><br>
    
    <strong>More Resources</strong><br>
    Mobile Speak and Mobile Magnifier are useful innovative applications designed for people with low vision. Both applications, such as many others available on the market can be used when accessing our website from a smartphone.
    </p>
                        </div>
        </div>
        <div class="row-fluid">
            <div class="span12">
                <p><span class="clsTitleText03" style="background-image:none"><strong>Contacting the Website</strong></span><br> If you have any questions about this website, you may contact us at:</p>			<p><strong>Address</strong></p>
                <div class="row padding_bottom10">
                    <div class="span4">
                        <p>{{$_SERVER['SERVER_NAME']}}</p>
                        4780 Ashford Dunwoody Rd, Suite A 236<br>Atlanta, GA, 30338<br>United States				</div>
                                </div>
                            <p>
                </p>
            </div>
        </div>
    </div>
            </div><!--end #content -->
        </div><!--end #wrapper -->
    </section>
@endsection

@extends('layouts.page')

@section('style')
<style>
        #stroke {
        fill: #a6a6a6;
        }

        .logotext,
        .logomark {
        fill: #ffffff;
        }

        .cls-4 {
        fill: url(#linear-gradient);
        }

        .cls-5 {
        fill: url(#linear-gradient-2);
        }

        .cls-6 {
        fill: url(#linear-gradient-3);
        }

        .cls-7 {
        fill: url(#linear-gradient-4);
        }

        .cls-8 {
        opacity: 0.2;
        }

        .top-Highlight, .cls-8, .cls-9 {
        isolation: isolate;
        }

        .cls-9 {
        opacity: 0.12;
        }

        .top-Highlight {
        opacity: 0.25;
        }
        body.homepage #wrapper{
            background-image: url('{{ url("/img/talkingMan.jpg") }}');
        }
        
    </style>
@endsection
@section('content')
<section id="topSection">
        <div id="wrapper" class="clearfix">
                @if(Auth::check())
                @if(Auth::user()->role = 'user')
                    @include('partials._submenu')
                @endif
                @endif
                    <div id="content">
                <style>
    
    </style>
    
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span12">
                <div class="banner-info pagination-centered">
                    <div class="container">
                        <h1>Top up a mobile phone in just 3 easy steps!</h1>
                        <p>An easy way to top up and recharge prepaid mobile phones worldwide!</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span12 padding20 margin_bottom60" style="background: rgba(255, 255, 255, 0.8); border-radius:10px;">
                <div class="row-fluid">
        <div class="span12 pagination-centered">
            <h2 class="margin_bottom30">Top up a mobile phone in just 3 easy steps!</h2>
        </div>
    </div>
    <div class="row-fluid pagination-centered">
        <div class="span3 margin_top30">
            <div class="feature-wrap">
                <i class="fa fa-mobile-phone" style="font-size: 52px;"></i>
                <h2>Recharge details</h2>
                <h3>Select the country, enter the phone number and choose the amount.</h3>
            </div>
            <select class='width100 margin_bottom20' id='international_country' aria-label='Select country'>
                <option value=''>Select country</option>
                @foreach($countries as $country)
                    <option value="{{$country->id}}" data-iso2="{{$country->iso2}}" data-id="{{$country->id}}" data-country_name="{{$country->name}}" data-phone-code="{{$country->phonecode}}">{{$country->name}}</option>
                @endforeach
			</select>
        </div>
        <div class="span1 margin_top30 hidden-phone"><img src="{{url('img/arrow-left.png')}}"></div>
        <div class="span4 margin_top30">
            <div class="feature-wrap">
                <i class="fa fa-money"></i>
                <h2>Payment details</h2>
                <h3>Enter your Credit or Debit Card details or your Paypal account.</h3>
            </div>
        </div>
        <div class="span1 margin_top30 hidden-phone"><img src="{{url('img/arrow-left.png')}}"></div>
        <div class="span3 margin_top30">
            <div class="feature-wrap">
                <i class="fa fa-thumbs-up"></i>
                <h2>Phone is Topped Up!</h2>
                <h3>Your top up is applied in real-time, and the mobile is ready to use!</h3>
            </div>
        </div>
    </div>			
            </div>
        </div>
    </div>
    
            </div><!--end #content -->
        </div><!--end #wrapper -->
    </section>

    <script>
            $(function() {
                $('#international_country').on('change', function() {
                    var selected_country = trim($(this).val());
                    if (selected_country !== '')
                        window.location = window.location.origin+'/user/recharge-now/'+selected_country
                });
            });
        </script>
@endsection
@extends('layouts.page')

@section('style')
<style>
    body.homepage header, body.how-to-top-up header{
		background-color: rgba(2,122,195,1);
		z-index: 1999;
    }
        #stroke {
        fill: #a6a6a6;
        }

        .logotext,
        .logomark {
        fill: #ffffff;
        }
        .cls-4 {fill: url(#linear-gradient);}
        .cls-5 {fill: url(#linear-gradient-2);}
        .cls-6 {fill: url(#linear-gradient-3);}
        .cls-7 {
        fill: url(#linear-gradient-4);
        }

        .cls-8 {
        opacity: 0.2;
        }

        .top-Highlight, .cls-8, .cls-9 {
        isolation: isolate;
        }

        .cls-9 {
        opacity: 0.12;
        }

        .top-Highlight {
        opacity: 0.25;
        }
    </style>
@endsection
@section('content')
<section id="topSection">
	<div id='wrapper' class='clearfix'>
		@if(Auth::check())
		@if(Auth::user()->role = 'user')
			@include('partials._submenu')
		@endif
		@endif
				<div id='content'>
			<div class='container-fluid color_white'>
	<div class='row-fluid'>
		<div class='span12 max_on_tablet' style="height:100%;text-align:center">
			<div class='span12 banner-info pagination-centered'>
				<h1 class='margin_top5' style="text-align: center;">Mobile Recharge Made Easy</h1>
			</div>
		</div>
	</div>
	<div class='row-fluid'>
		<div class='span1'></div>
		<div class='span5 max_on_tablet' style="height:100%">
			<div class='span12 banner-info pagination-centered' style="margin-top: -50px;">
				<h2 style="font-size: 40px;">400+ Mobile Carriers</h2>
				<h2 style="font-size: 40px;">140+ Countries</h2>
			</div>
		</div>
		<div class='span5 max_on_tablet' style="margin-top:20px">
			<div class='pagination-centered'>
				<select class='width100 margin_bottom20' id='international_country' aria-label='Select country'>
					<option value=''>Select country</option>
					@foreach($countries as $country)
						<option value="{{$country->id}}" data-iso2="{{$country->iso2}}" data-id="{{$country->id}}" data-country_name="{{$country->name}}" data-phone-code="{{$country->phonecode}}">{{$country->name}}</option>
					@endforeach
				</select>
				<a href='{{url('/user/recharge-now/')}}' class='btn btn-primary padding_horizontal50 margin_bottom20'>Recharge Now &raquo;</a><br>
			</div>
			<img src='/images/spinner.gif' class='select_spinner none'>
			<div>
				<div class='country_selected none' style="height:221px">
					<p class='color_white font-size18'>Select operator</p>
					<div class='margin_bottom10 operators clearfix'></div>
				</div>
				<div class='margin_bottom10 pagination-centered' style="height:21px">
					<a href='/buy/mobile_recharge' class='none more_operators color_white'>More Networks &raquo;</a>
				</div>
			</div>
						<div class='pagination-centered margin_bottom30 margin_top20'>
				<div class='pagination-centered margin_bottom20'>
					<h3>An easy way to top up and recharge prepaid mobile phones worldwide!</h3>
				</div>
			</div>
		</div>
	</div>
</div>
<section id='feature'>
	<div class='container-fluid'>
		<div class='row-fluid'>
	<div class='span12 pagination-centered'>
		<h2 class='margin_bottom30'>Top up a mobile phone in just 3 easy steps!</h2>
	</div>
</div>
<div class='row-fluid pagination-centered'>
	<div class='span3 margin_top30'>
		<div class='feature-wrap'>
			<i class='fa fa-phone' style='font-size: 52px;'></i>
			<h2>Recharge details</h2>
			<h3>Select the country, enter the phone number and choose the amount.</h3>
		</div>
			</div>
	<div class='span1 margin_top30 hidden-phone'><img src='{{url('img/arrow-left.png')}}'></div>
	<div class='span4 margin_top30'>
		<div class='feature-wrap'>
		<i class="fa fa-dollar"></i>
			<h2>Payment details</h2>
			<h3>Enter your Credit or Debit Card details or your Paypal account.</h3>
		</div>
	</div>
	<div class='span1 margin_top30 hidden-phone'><img src='{{url('img/arrow-left.png')}}'></div>
	<div class='span3 margin_top30'>
		<div class='feature-wrap'>
			<i class='fa fa-check-circle'></i>
			<h2>Phone is Topped Up!</h2>
			<h3>Your top up is applied in real-time, and the mobile is ready to use!</h3>
		</div>
	</div>
</div>	</div>
</section>
	{{-- <section id='bottomSection' class='hidden-phone'>
		<div id='bottomSection-tint'>
			<div class='container-fluid'>
				<div class='row-fluid'>
					<div class='span12'>
						<div class='pagination-centered'>
							<h2 class='color_white'>Get the latest specials, promotions, and news for prepaid mobile carriers worldwide</h2>
						</div>

						<div id='feed-box' style='overflow-y: hidden; height: 240px;'>
							<style>
								.offer_description {display: block}
							</style>
									<div
			data-country='MX'
			class='offer1 padding10 border_box margin_vertical10 position_relative offer_description'
		>
			<div class='row-fluid position_relative mr_offer_container'>
									<div class="pull-left position_relative offer_box pagination-centered ">
						<div class='operator_logo operator_name_telcel_new'></div>
						<div class='box_bottom_gradient'>
							<div class='margin_horizontal10'>
								<strong>2x Bonus</strong> for <strong>Telcel Paquetes</strong>							</div>
						</div>
						<a href="/buy/mobile_recharge?country=Mexico&amp;operator=Telcel+Bundles" class='banner_shadow_box'></a>
					</div>
								<div class='banner_corner'></div>
			</div>
		</div>
			<div
			data-country='NG'
			class='offer1 padding10 border_box margin_vertical10 position_relative offer_description'
		>
			<div class='row-fluid position_relative mr_offer_container'>
									<div class="pull-left position_relative offer_box pagination-centered ">
						<div class='operator_logo operator_name_9mobile'></div>
						<div class='box_bottom_gradient'>
							<div class='margin_horizontal10'>
								<b>900%</b> Bonus for <b>9Mobile</b>							</div>
						</div>
						<a href="/buy/mobile_recharge?country=Nigeria&amp;operator=9Mobile+Prepaid+Credit" class='banner_shadow_box'></a>
					</div>
								<div class='banner_corner'></div>
			</div>
		</div>
	
						<div class='click-here'>
							<div class='row'>
								<div align='center'>
									<a href='/promotions' class='btn btn-info btn-lg'>View All Promos</a></div>
							</div>
						</div>
						<!--News End-->
					</div>
				</div>
			</div>
		</div>
	</section> --}}
<script>
	$(function() {
		$('#international_country').on('change', function() {
			var selected_country = trim($(this).val());
			if (selected_country !== '')
				window.location = window.location.origin+'/user/recharge-now/'+selected_country
		});
	});
</script>
		</div><!--end #content -->
	</div><!--end #wrapper -->
</section>
@endsection
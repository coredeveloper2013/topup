@foreach($products->chunk(3) as $key => $prodChunk)
    <div class="row-fluid">
        @foreach($prodChunk as $key => $prod)
            <div recharge-type="fixed" class="span4 border_all pagination-centered font-size24 mr_preffered_rate deselected ammountToRecharge {{ $key > 5 ? 'gretterSIx': 'inSix' }}" style="display:{{ $key > 5 ? 'none': 'block' }}" data-rate="{{ $prod->vendorPrice['price'] ? $prod->vendorPrice['price'] : $prod->price }}" data-id="{{ $prod->id }}" data-product="{{ $prod->product }}" data-currency="{{ $prod->pcur }}">{{ $prod->pcur }} {{ $prod->product }}</div>
        @endforeach
    </div>
@endforeach
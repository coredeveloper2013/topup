@foreach($operators as $key => $ope)
@php
    $name = explode(' ', $ope->name);

    unset($name[count($name) - 1]);
    $nameImp = implode(' ', $name);
    $nameLower = str_replace(' ','',$nameImp);

    $color = substr(md5(rand()), 0, 6);
@endphp
    <div class="operator thisOperator" data-operator_name="{{strtolower($nameLower)}}"  data-operator_fullname="{{$ope->name}}" data-operator_id="{{$ope->id}}" style="height:90px;background-color:#{{$color}}">
        <div class="operator_name_asa" style="color:white;"><strong>{{$ope->name}}</strong></div>
    </div>
@endforeach
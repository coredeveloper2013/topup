<nav id="products_nav" role="navigation" style="padding-top: 12px">
    <div class="container-fluid border_box submenu" style="background-color:#133F8A">
        <div class="row-fluid mobile_pagination_left pagination-right">
            @if(Auth::check())
            @if(in_array('vendor', explode(',', Auth::user()->role)))
                <a href="{{route('vendorHome')}}" class="{{Request::is('vendor/home') ? 'active' : null}}">Home</a>
                <a href="{{route('vendorUsers')}}" class="{{Request::is('vendor/users') ? 'active' : null}}">Users</a>
                <a href="{{route('vendorPricing')}}" class="{{Request::is('vendor/set-price') ? 'active' : null}}">Pricing</a>
                <a href="{{route('vendorTransactions')}}" class="{{Request::is('vendor/transaction') ? 'active' : null}}">Transaction</a>
                <a href="{{route('vendorCredentials')}}" class="{{Request::is('vendor/credentials') ? 'active' : null}}">Credentials</a>
                <a href="{{route('vendorInformation')}}" class="{{Request::is('vendor/information') ? 'active' : null}}">Manage</a>
            @elseif(in_array('user', explode(',', Auth::user()->role)))
                <a href="{{route('userHome')}}" class="{{Request::is('user/home') ? 'active' : null}}">Home</a>
                <a href="{{route('userTransaction')}}" class="{{Request::is('user/transaction') ? 'active' : null}}">Transaction</a>
                <a href="{{route('userPocket')}}" class="{{Request::is('user/pocket') ? 'active' : null}}">Pocket</a>
                <a href="{{route('userInformation')}}" class="{{Request::is('user/information') ? 'active' : null}}">Manage</a>
            @elseif(in_array('admin', explode(',', Auth::user()->role)))
                <a href="{{route('adminCreateVendor')}}" class="{{Request::is('admin/vendor') ? 'active' : null}}">Vendor</a>
                <a href="{{route('adminInformation')}}" class="{{Request::is('admin/information') ? 'active' : null}}">Manage</a>
            @endif
            @endif
        </div>
    </div>
</nav>